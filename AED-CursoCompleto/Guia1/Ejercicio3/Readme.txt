->Ejercicio 3

Al ejecutar el programa, se sabe por defecto la clave, la extensión, ubicación, precio y disponibilidad de los deptos. Es por eso que el programa cuenta con un menú, el cual permite realizar las siquientes operaciones:
	a) Lista los datos de todos los deptos disponibles que tengan un precio inferior o igual a cierto valor P.
	b) Liste los datos de los deptos disponibles que tengan una superficie mayor o igual a un cierto valor dado E y una ubicación excelente.
	c) Liste el monto del arriendo de todos los deptos arrendados.
	d) Arrendar un depto. Con caracteristicas deseadas, con precio y ubicación que se ajustan a las necesidades del cliente, el depto de le
	arrendará.
	e) Actualizar los precios de los arriendos de los deptos no alquilados.
Tener mucho cuidado con ingresar valores string, donde se solicita un valor numérico.
El programa se compila utilizando el comando make y se ejecuta con ./main

->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

