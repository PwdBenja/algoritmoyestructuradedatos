#include <iostream>
using namespace std;

#ifndef DEPARTAMENTO_H
#define DEPARTAMENTO_H

class Departamento {
    private:
		string ubicacion = "\0";
		int extension;
		int clave;
		int precio;
		bool disponible;
       
    public:
    
		/* constructor */
		Departamento ();
		/* get y setter ubicacion*/
		string get_ubicacion();
		void set_ubicacion(string ubicacion);
		/* get y setter extencion */
		int get_extension();
		void set_extension(int extension);
		/* get y setter clave */
		int get_clave();
		void set_clave(int clave);
		/* get y setter precio */
		int get_precio();
		void set_precio(int precio);
		/* get y setter disponible */
		bool get_disponible();
		void set_disponible(bool disponible);
};
#endif
