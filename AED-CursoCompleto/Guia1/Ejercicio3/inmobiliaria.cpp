#include "Departamento.h"
#include <iostream>
#include <time.h>

using namespace std;
	/*Una inmobiliaria tiene información sobre departamentos en arriendo. De cada departamento se conoce:
	a) Clave: es un entero que identifica al inmueble.
	b) Extensión: superficie del depto en metros cuadrados.
	c) Ubicación: (excelente, buena, regular, mala).
	d ) Precio: es un entero.
	e) Disponible: verdadero o falso.
	Diariamente acuden muchos clientes a la inmobiliaria solicitando información. Escriba un programa
	capaz de realizar las siguientes operaciones sobre la información disponible:
	a) Liste los datos de todos los deptos disponibles que tengan un precio inferior o igual a cierto valor
	P.
	b) Liste los datos de los deptos disponibles que tengan una superficie mayor o igual a un cierto
	valor dado E y una ubicación excelente.
	c) Liste el monto del arriendo de todos los deptos arrendados.
	d ) Llega un cliente solicitando arrendar un depto. Si existe alguno con una superficie mayor o igual
	a la deseada, con precio y ubicación que se ajustan a las necesidades del cliente, el depto de le
	arrendará.
	e) Se ha decidido aumentar el arriendo en un X %. Actualizar los precios de los arriendos de los
	deptos no alquilados. */

/* imprimir actual lista de departamentos */
void imprimirLista(int tipoFiltro, int p, int numero, Departamento depas[]){
	/* contador opcion 3, la función es encargarse de que se imprima un mensaje en caso
	   de  que todas las casas esten disponibles */
	int contador = 0;	
	for (int h = 0; h < numero; h++){
		/* filtro opcion 1*/
		if (depas[h].get_precio() <= p && tipoFiltro == 1){
			cout << "------------------------" << endl;
			cout << "Clave: " << depas[h].get_clave() << endl;
			cout << "Extencion: "  << depas[h].get_extension() << endl;
			cout << "Ubicación: " << depas[h].get_ubicacion() << endl;
			cout << "Precio: "<< depas[h].get_precio() << endl;
			/* comprobar disponibilidad */
			if (depas[h].get_disponible() == true ){
				cout << "Disponibilidad: Si" << endl;
			}
			else {
				cout << "Disponibilidad: No" << endl;
			}
			cout << "------------------------" << endl;
		}
		/*filtro opcion 2*/
		if (depas[h].get_extension() <= p && tipoFiltro == 2 && depas[h].get_ubicacion() == "Excelente"){
			cout << "------------------------" << endl;
			cout << "Clave: " << depas[h].get_clave() << endl;
			cout << "Extencion: "  << depas[h].get_extension() << endl;
			cout << "Ubicación: " << depas[h].get_ubicacion() << endl;
			cout << "Precio: "<< depas[h].get_precio() << endl;
			/* comprobar disponibilidad */
			if (depas[h].get_disponible() == true ){
				cout << "Disponibilidad: Si" << endl;
			}
			else {
				cout << "Disponibilidad: No" << endl;
			}
			cout << "------------------------" << endl;
		}
		
		/* filtro opcion 3 */
		if (tipoFiltro == 3 && depas[h].get_disponible() == true ){
			contador++;
			if (depas[h].get_disponible() == false){
				cout << "------------------------" << endl;
				cout << "Clave: " << depas[h].get_clave() << endl;
				cout << "Extencion: "  << depas[h].get_extension() << endl;
				cout << "Ubicación: " << depas[h].get_ubicacion() << endl;
				cout << "Precio: "<< depas[h].get_precio() << endl;
				/* comprobar disponibilidad */
				if (depas[h].get_disponible() == true ){
					cout << "Disponibilidad: Si" << endl;
				}
				else {
					cout << "Disponibilidad: No" << endl;
				}
				cout << "------------------------" << endl;
			}
			else if (contador == numero){
				cout << "Todas las casas estan disponibles, alquile una. " << endl;
				contador = 0;
			}
		}
		if (tipoFiltro == 5){
			cout << "------------------------" << endl;
			cout << "Clave: " << depas[h].get_clave() << endl;
			cout << "Extencion: "  << depas[h].get_extension() << endl;
			cout << "Ubicación: " << depas[h].get_ubicacion() << endl;
			cout << "Precio: "<< depas[h].get_precio() << endl;
			/* comprobar disponibilidad */
			if (depas[h].get_disponible() == true ){
				cout << "Disponibilidad: Si" << endl;
			}
			else {
				cout << "Disponibilidad: No" << endl;
			}
			cout << "------------------------" << endl;
		}
	}
}


/* Función que rellena el arreglo con departamentos y su información */
void listaDepas(int numero, Departamento depas[]){
	for (int i = 0; i < numero; i++){
		/*Excelente*/
		if (i == 0){
			depas[i].set_clave(1);
			depas[i].set_extension(100);
			depas[i].set_ubicacion("Excelente");
			depas[i].set_precio(100);
			depas[i].set_disponible(true);
		}
		/*bueno*/
		if (i == 1){
			depas[i].set_clave(2);
			depas[i].set_extension(200);
			depas[i].set_ubicacion("Bueno");
			depas[i].set_precio(200);
			depas[i].set_disponible(false);
		}
		/*regular*/
		if (i == 2){
			depas[i].set_clave(3);
			depas[i].set_extension(300);
			depas[i].set_ubicacion("Regular");
			depas[i].set_precio(300);
			depas[i].set_disponible(true);		
		}
		/*malo*/	
		if (i == 3){
			depas[i].set_clave(4);
			depas[i].set_extension(400);
			depas[i].set_ubicacion("Malo");
			depas[i].set_precio(400);
			depas[i].set_disponible(true);
		}	
	}
}

/* Primer filtro, se filtra por el valor p */
/* Liste los datos de todos los deptos disponibles que tengan un precio inferior o igual a cierto valor p */
void opcionUno(int numero, Departamento depas[]){
	int p = 0;
	cout << "Ingrese el dinero que tiene: " << endl;
	cin >> p;
	int tipoFiltro = 1;
	imprimirLista(tipoFiltro, p, numero, depas);
}

 /* Lista los datos de los deptos disponibles que tengan una superficie mayor o igual a un cierto
	valor dado E y una ubicación excelente. */
void opcionDos(int numero, Departamento depas[]){
	int e = 0;
	int tipoFiltro = 2;
	cout << "Ingrese los metros cuadrados que necesita: " << endl;
	cin >> e;
	imprimirLista(tipoFiltro, e, numero, depas);
}

/* Lista el monto del arriendo de todos los deptos arrendados. */
void opcionTres(int numero, Departamento depas[]){
	int e = 0;
	int tipoFiltro = 3;
	imprimirLista(tipoFiltro, e, numero, depas);
}

/*	Llega un cliente solicitando arrendar un depto. Si existe alguno con una superficie mayor o igual
	a la deseada, con precio y ubicación que se ajustan a las necesidades del cliente, el depto de le
	arrendará.*/
void opcionCuatro(int numero, Departamento depas[]){
	int precioDeseado = 0;
	string ubicacionDeseada = "\0";
	int superficieDeseada;
	int claveCompra;
	int contador = 0;
	cout << "Ingrese precio que le acomode:" << endl;
	cin >> precioDeseado;
	cout << "Ubicacion |Excelente -> 1||Bueno -> 2||Regular -> 3||Malo -> 4|" << endl;
	cin >> ubicacionDeseada;
	cout << "Superficie deseada: " << endl;
	cin >> superficieDeseada;
	for (int h = 0; h < numero; h++){
		/* Buscando similitudes */
		if (depas[h].get_precio() < precioDeseado && depas[h].get_disponible() == true){
			contador ++;
			cout << "------------------------" << endl;
			cout << "Clave: " << depas[h].get_clave() << endl;
			cout << "Extencion: "  << depas[h].get_extension() << endl;
			cout << "Ubicación: " << depas[h].get_ubicacion() << endl;
			cout << "Precio: "<< depas[h].get_precio() << endl;
			/* comprobar disponibilidad */
			if (depas[h].get_disponible() == true ){
				cout << "Disponibilidad: Si" << endl;
			}
			else {
				cout << "Disponibilidad: No" << endl;
			}
			cout << "------------------------" << endl;
		}
	}
	if (contador != 0){
		cout << "Ingrese CLAVE de la que desea arrendar:" << endl;
		cin >> claveCompra;
		for (int h = 0; h < numero; h++){
			if (depas[h].get_clave() == claveCompra){
				depas[h].set_disponible(false);
				if (depas[h].get_disponible() == false ){
					cout << "Disponibilidad: No  [ACTUALIZADO]" << endl;
				}
			}
		}
	}
 }
 
/*	Actualizar los precios de los arriendos de los
	deptos no alquilados. */
void opcionCinco(int numero, Departamento depas[]){
	int tasaCambio = 1+rand()%(101-1);
	int tipoFiltro = 5;
	int e = 0; 
	int aux = 0;
	/* Imprimir lista  actual */
	imprimirLista(tipoFiltro, e, numero, depas);
	for (int h = 0; h < numero; h++){
		if ( depas[h].get_disponible() == true ){
			aux = depas[h].get_precio();
			aux = aux * tasaCambio;
			depas[h].set_precio(aux);
		}
	}
	/*Se limpia la terminal */
	system("clear");
	/* Lista con el precio aumeentado */
	cout << "[ACTUALIZADO]" << endl;
	imprimirLista(tipoFiltro, e, numero, depas);
}

/* opciones de diferentes operaciones que podrá acudir el cliente */
/* Menú */
void menuCliente(int numero, Departamento depas[]){
	string opcion;
	cout << "Tipos de filtros:" << endl;
	cout << "--> Filtrar precios [1] " << endl;
	cout << "--> Filtrar por metros cuadrados [2] " << endl;
	cout << "--> Mostrar precios de arriendo de los deptos ya arrendados [3] " << endl;
	cout << "--> ¿Desea arrendar? [4]" << endl;
	cout << "--> Actualizar arrienda [5]" << endl;
	cin >> opcion;
	if (opcion == "1"){
		opcionUno(numero, depas);
	}
	else if (opcion == "2"){
		opcionDos(numero, depas);	
	}
	else if (opcion == "3"){
		opcionTres(numero, depas);	
	}
	else if (opcion == "4"){
		opcionCuatro(numero, depas);	
	}
	else if (opcion == "5"){
		opcionCinco(numero, depas);	
	}
	cin.ignore(); /* Limpieza de buffer */
}

/* */
int main (){
	srand(time(NULL));
	int numero = 4;
	bool auxiliar = true;
	/* Arreglo de objetos */
	Departamento depas[numero];
	listaDepas(numero, depas);
	/* Se llama al menú del programa */
	menuCliente(numero, depas);
	while (auxiliar){
		string opcion = "\0";
		cout << "¿Desea realizar otra acción?: S [seguir], seleccione una tecla[salir]" << endl;
		cin >> opcion;
		if (opcion == "s" || opcion == "S"){
				menuCliente(numero, depas);
		}
		else {
			return 0;
		}
	}
	return 0;
}
