#include "Departamento.h"
#include <iostream>
using namespace std;


Departamento::Departamento(){
	
}

/* Get y Set de ubicacion */
string Departamento::get_ubicacion() {
    return this->ubicacion;
}

void Departamento::set_ubicacion(string ubicacion) {
    this->ubicacion = ubicacion;
}

/* Get y Set de extension */
int Departamento::get_extension() {
    return this->extension;
}

void Departamento::set_extension(int extension) {
    this->extension = extension;
}

/* Get y Set de clave */
int Departamento::get_clave() {
    return this->clave;
}

void Departamento::set_clave(int clave) {
    this->clave = clave;
}

/* Get y Set de precio */
int Departamento::get_precio() {
    return this->precio;
}

void Departamento::set_precio(int precio) {
    this->precio = precio;
}

/* Get y Set de disponible */
bool Departamento::get_disponible() {
    return this->disponible;
}

void Departamento::set_disponible(bool disponible) {
    this->disponible = disponible;
}

