#include "Paciente.h"
#include <iostream>
using namespace std;


Paciente::Paciente(){
	
}

/* Get y Set de nombre */
string Paciente::get_nombre() {
    return this->nombre;
}

void Paciente::set_nombre(string nombre) {
    this->nombre = nombre;
}

/* Get y Set de sexo */
string Paciente::get_sexo() {
    return this->sexo;
}

void Paciente::set_sexo(string sexo) {
    this->sexo = sexo;
}

/* Get y Set de edad */
int Paciente::get_edad() {
    return this->edad;
}

void Paciente::set_edad(int edad) {
    this->edad = edad;
}

/* Get y Set de telefono */
string Paciente::get_telefono() {
    return this->telefono;
}

void Paciente::set_telefono(string telefono) {
    this->telefono = telefono;
}

/* Get y Set de seguro */
bool Paciente::get_seguro() {
    return this->seguro;
}

void Paciente::set_seguro(bool seguro) {
    this->seguro = seguro;
}

/* Get y Set de domicilio */
string Paciente::get_domicilio() {
    return this->domicilio;
}

void Paciente::set_domicilio(string domicilio) {
    this->domicilio = domicilio;
}
