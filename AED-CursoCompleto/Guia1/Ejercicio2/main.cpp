#include "Paciente.h"
#include <iostream>
using namespace std;

/* 	Al momento de su ingreso al hospital, a un paciente se le solicitan los siguientes datos: Nombre, Edad,
	Sexo, Domicilio (Calle, número, ciudad), Teléfono, Seguro médico (verdadero o falso).
	Escriba un programa que pueda llevar a cabo las siguientes operaciones:
	
	a) Listar los nombres de todos los pacientes hospitalizados.
	b) Obtener el porcentaje de pacientes hospitalizados en las siguientes categorı́as (dadas por la edad):
		1) Niños: hasta 13 años.
		2) Jóvenes: mayores de 13 años y menores de 30.
		3) Adultos: mayores de 30 años.
	c) Obtener el porcentaje de hombres y de mujeres hospitalizadas.
	d ) Dado el nombre del paciente, listar todos los datos relacionados con dicho paciente.
	e) Calcular el porcentaje de pacientes que poseen seguro médico. */

//~ void agregarPaciente(int numero, Paciente pacientes[]){
	//~ numero = numero + 1;
	//~ string calle = "\0";
	//~ string nuumero = "\0";
	//~ string ciudad = "\0";
	
	
	//~ for (int i = 0; i < numero; i++){
		//~ if (i == numero - 1 ){
			//~ pacientes[numero].set_nombre("Pangui");
			//~ pacientes[numero].set_sexo("M");
			//~ pacientes[numero].set_edad(10);
			//~ calle = "hermosa";
			//~ nuumero = "123";
			//~ ciudad = "santiago";
			//~ pacientes[numero].set_domicilio(calle + "," + nuumero + "," + ciudad);
			//~ pacientes[numero].set_telefono("+56912345678");
			//~ pacientes[numero].set_seguro(false);
		//~ }
	//~ }
//~ }

/* Función que permite ver los datos detallados del paciente ingresado por pantalla */
void seleccionPaciente(int numero, Paciente pacientes[]){
	int contador = 0;
	string ayudantePaciente = "\0";
	cout << "Escriba el paciente a buscar: " << endl;
	cin >> ayudantePaciente;
	system("clear");
	for(int i = 0; i< numero; i++){
		if (pacientes[i].get_nombre() == ayudantePaciente){
			contador++;
			cout << "[Nombre]: " << pacientes[i].get_nombre() << endl;
			cout << "[Sexo]: " << pacientes[i].get_sexo() << endl;
			cout << "[Edad]: " << pacientes[i].get_edad() << endl;
			cout << "[Domicilio]: " << pacientes[i].get_domicilio() << endl;
			cout << "[Telefono]: " <<pacientes[i].get_telefono() << endl;
			if (pacientes[i].get_seguro() == true ){
				cout << "[seguro]: Si" << endl;
			}
			else {
				cout << "[seguro]: No" << endl;
			}		
		}
	}
	if (contador == 0) {
		cout << "El paciente [" << ayudantePaciente << "] No existe" << endl;
	}
		
}

void porcentajeSeguro(int numero, Paciente pacientes[] ){
	/* contadores */
	int seguro = 0;
	for(int i = 0; i< numero; i++){
		if (pacientes[i].get_seguro() == true){
			seguro++;
		}
	}
	/* calculando porcentajes */
	seguro = (seguro*100)/numero;
	/* imprimiendo */ 
	cout << "El porcentaje de pacientes con seguro es: %" << seguro << endl;
}

/* función que calcula el porcentaje de mujeres y de hombres hospitalizados */
void porcentajeHombreMujer(int numero, Paciente pacientes[] ){
	/* contadores */
	int mujeres = 0;
	int hombres = 0;
	for(int i = 0; i< numero; i++){
		if (pacientes[i].get_sexo() == "F"){
			mujeres++;
		}
		if (pacientes[i].get_sexo() == "M"){
			hombres++;
		}
	}
	/* calculando porcentajes */
	mujeres = (mujeres*100)/numero;
	hombres = (hombres*100)/numero;
	/* imprimiendo */
	cout << "El porcentaje de mujeres hospitalizados es: %" << mujeres << endl;
	cout << "El porcentaje de hombres hospitalizados es: %" << hombres << endl;
}

/* Calculo de porcentaje de niños,jóvenes y adultos hospitalizados*/
void imprimirPorcentajes(int numero, Paciente pacientes[]){
	/* Contador */
	int contadorNino = 0;
	int contadorJovenes = 0;
	int contadorAdultos = 0;
	
	for(int i = 0; i< numero; i++){
		if (pacientes[i].get_edad() <= 13)  {
			contadorNino++;
		}
		if (pacientes[i].get_edad() > 13 && pacientes[i].get_edad() < 30)  {
			contadorJovenes++;
		}
		if (pacientes[i].get_edad() >= 30)  {
			contadorAdultos++;
		}		
	}
	
	/* Calcular porcentajes */
	contadorNino = (contadorNino*100)/numero;
	contadorJovenes = (contadorJovenes*100)/numero;
	contadorAdultos = (contadorAdultos*100)/numero;
	/* Imprimir porcentajes */
	cout << "El porcentaje de niños hospitalizados es: %" << contadorNino << endl;
	cout << "El porcentaje de jovenes hospitalizados es: %" << contadorJovenes << endl;
	cout << "El porcentaje de Adultos hospitalizados es: %" << contadorAdultos << endl;
}

/* imprime todos los pacientes del hospital */
void imprimirPacientes(int numero, Paciente pacientes[]){
	
	/* pacientes */
	cout << "Pacientes: " << endl;
	for(int i = 0; i< numero; i++){
		cout <<	pacientes[i].get_nombre() << endl;
	}
}

/* Función que registra los pacientes. */
void registropacientes(int numero, Paciente pacientes[]){
	string calle = "\0";
	string nuumero = "\0";
	string ciudad = "\0";
	for(int i = 0; i< numero; i++){
		if (i == 0){
			pacientes[i].set_nombre("Benja");
			pacientes[i].set_sexo("M");
			pacientes[i].set_edad(10);
			calle = "hermosa";
			nuumero = "123";
			ciudad = "santiago";
			pacientes[i].set_domicilio(calle + "," + nuumero + "," + ciudad);
			pacientes[i].set_telefono("+56912345678");
			pacientes[i].set_seguro(false);
		}
		else if (i == 1){
			pacientes[i].set_nombre("vale");
			pacientes[i].set_sexo("F");
			pacientes[i].set_edad(21);
			calle = "hermosa";
			nuumero = "123";
			ciudad = "santiago";
			pacientes[i].set_domicilio(calle + "," + nuumero + "," + ciudad);
			pacientes[i].set_telefono("+56912345678");
			pacientes[i].set_seguro(true);
		}
		else if (i == 2){
			pacientes[i].set_nombre("Nicole");
			pacientes[i].set_sexo("F");
			pacientes[i].set_edad(19);
			calle = "hermosa";
			nuumero = "123";
			ciudad = "santiago";
			pacientes[i].set_domicilio(calle + "," + nuumero + "," + ciudad);
			pacientes[i].set_telefono("+56912345678");
			pacientes[i].set_seguro(true);		
		}
		else if (i == 3){
			pacientes[i].set_nombre("Marcela");
			pacientes[i].set_sexo("F");
			pacientes[i].set_edad(46);
			calle = "hermosa";
			nuumero = "123";
			ciudad = "santiago";
			pacientes[i].set_domicilio(calle + "," + nuumero + "," + ciudad);
			pacientes[i].set_telefono("+56912345678");
			pacientes[i].set_seguro(true);
		}
		else if (i == 4){
			pacientes[i].set_nombre("joaquin");
			pacientes[i].set_sexo("M");
			pacientes[i].set_edad(66);
			calle = "hermosa";
			nuumero = "123";
			ciudad = "santiago";
			pacientes[i].set_domicilio(calle + "," + nuumero + "," + ciudad);
			pacientes[i].set_telefono("+56912345678");
			pacientes[i].set_seguro(false);
		}
	}
}

int main(void){
	int numero = 5;
	bool zapato = true;
	/* Se crea el arreglo de clases */
	Paciente pacientes[numero];
	/* llamado de funciones */
	registropacientes(numero, pacientes);	
	imprimirPacientes(numero, pacientes);
	imprimirPorcentajes(numero, pacientes);
	porcentajeHombreMujer(numero, pacientes);
	porcentajeSeguro(numero, pacientes);
	seleccionPaciente(numero, pacientes);
	while (zapato){
		string opcion;
		cout << "¿Desea ver otro Paciente? Presione cualquier letra para salir, [S] para continuar" << endl;
		cin >> opcion;
		if (opcion == "s" || opcion == "S"){
			seleccionPaciente(numero, pacientes);
		}
		else {
			zapato = false;
		}
	}
	// agregarPaciente(numero, pacientes);
	return 0;
}
