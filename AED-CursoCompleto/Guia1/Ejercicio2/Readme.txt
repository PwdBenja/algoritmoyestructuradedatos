->Ejercicio 2

Dentro del código se encuentra el registro de los pacientes, el programa logrará ordenar de diferentes maneras la información almacenada en cada una de ellas, calculando porcentajes, además de poder buscar la información específica sobre algun paciente. 
se compila con ./main

-El programa se compila utilizando el comando make y se ejecuta con ./main
->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

