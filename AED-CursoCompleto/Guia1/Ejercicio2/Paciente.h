#include <iostream>
using namespace std;

#ifndef PACIENTE_H
#define PACIENTE_H

class Paciente {
    private:
		string nombre = "\0";
		string sexo = "\0";
		int edad;
		string telefono = "\0";
		bool seguro;
		/* Domicilio */
		string domicilio = "\0";
       
    public:
    
		/* constructor */
		Paciente ();
		/* get y setter nombre*/
		string get_nombre();
		void set_nombre(string nombre);
		/* get y setter sexo */
		string get_sexo();
		void set_sexo(string sexo);
		/* get y setter edad */
		int get_edad();
		void set_edad(int edad);
		/* get y setter telefono */
		string get_telefono();
		void set_telefono(string telefono);
		/* get y setter seguro */
		bool get_seguro();
		void set_seguro(bool seguro);
		/* get y setter domicilio */
		string get_domicilio();
		void set_domicilio(string domicilio);
};
#endif
