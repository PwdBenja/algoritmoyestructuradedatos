#include "Trabajador.h"
#include <iostream>
using namespace std;
/* Constructor */
Trabajador::Trabajador(){
	
}

/* Get y Set de nombre */
string Trabajador::get_nombre() {
    return this->nombre;
}

void Trabajador::set_nombre(string nombre) {
    this->nombre = nombre;
}

/* Get y Set de sexo */
string Trabajador::get_sexo() {
    return this->sexo;
}

void Trabajador::set_sexo(string sexo) {
    this->sexo = sexo;
}

/* Get y Set de edad */
int Trabajador::get_edad() {
    return this->edad;
}

void Trabajador::set_edad(int edad) {
    this->edad = edad;
}
