#include "Trabajador.h"
#include <iostream>
using namespace std;

/* 	El departamento de personal de la Universidad tiene registros del nombre, sexo y edad de cada uno de
	los profesores (debe leer esta información). Escriba un programa que calcule e imprima los siguientes datos:
	
	a) Edad promedio del grupo de profesores.
	b) Nombre del profesor más joven.
	c) Nombre del profesor de más edad.
	d) Número de profesoras con edad mayor al promedio.
	e) Número de profesores con edad menor al promedio.
	
	Utilice combinación de Arreglos y Clases en su solución. */

/*Función que imprime la información de los trabajadores */
void impresionTrabajador(int numero, Trabajador trabajadores[]){
	cout << "Trabajadores de la universidad : " << endl;
	for(int i = 0; i< numero; i++){			
		cout << "[Nombre]: " << trabajadores[i].get_nombre() << endl;
		cout << "[Sexo]: " << trabajadores[i].get_sexo() << endl;
		cout << "[Edad]: " << trabajadores[i].get_edad() << endl;
		cout << "-----------" << endl;
	}
	
}

/* función que muestra el numero de trabajadares con una edad menor al promedio */
void hombreMenorPromedio(int promedio, int numero, Trabajador trabajadores[]){
	cout << "Trabajadores con una edad menor al promedio : " << endl;
	for(int i = 0; i< numero; i++){
		if ((trabajadores[i].get_sexo() == "M") && (trabajadores[i].get_edad() < promedio)){
			cout << trabajadores[i].get_nombre() << endl;
		}
	}
	cout << "    " << endl;
}

/* función que muestra el numero de trabajadaras con una edad mayor al promedio */
void mujerMayorPromedio(int promedio, int numero, Trabajador trabajadores[]){
	cout << "Trabajadoras con una edad mayor al promedio : " << endl;
	for(int i = 0; i< numero; i++){
		if ((trabajadores[i].get_sexo() == "F") && (trabajadores[i].get_edad() > promedio)){
			cout << trabajadores[i].get_nombre() << endl;
		}
	}
	cout << "    " << endl;
}

/* Función para encontrar al trabajador más joven y al más longevo.*/
void trabajadorJoven(int numero, Trabajador trabajadores[]){
	    /* Método burbuja */	    
	    Trabajador aux; /* Objeto auxiliar */
        bool centinela=true;
        for(int i = 0;i < numero && centinela;i++){
            centinela = false;
            for(int j = 1;j < numero;j++){
				/* Ordena de menor a mayor el arreglo */
                if(trabajadores[j-1].get_edad() > trabajadores[j].get_edad()){
                    aux = trabajadores[j];
                    trabajadores[j] = trabajadores[j-1];
                    trabajadores[j-1] = aux;
                    centinela=true;
                }
            }
		}
		/* Se imprime el primer objeto del arreglo, por tanto el trabajador de menor edad.*/
		cout << "Trabajador de menor edad: "<< trabajadores[0].get_nombre() << endl;
		/* Se imprime el ultimo objeto del arreglo, por tanto el trabajador de mayor edad.*/
		cout << "Trabajador de mayor edad: "<< trabajadores[numero - 1].get_nombre() << endl;
}

/* función encargada de calcular el valor de la edad promedio de los trabajadores */
int edadPromedio(int numero, Trabajador trabajadores[]){
	int sumaEdad = 0;
	for(int i = 0; i< numero; i++){
		sumaEdad = sumaEdad + trabajadores[i].get_edad();
	}
	/* Se calcula el promedio */
	sumaEdad = sumaEdad/numero;
	/* Se informa del promedio de los trabajadores */
	cout << "La edad promedio de los trabajadores es: " << sumaEdad << endl;
	return sumaEdad;
}

/* Función que registra los trabajadores. */
void registroTrabajadores(int numero, Trabajador trabajadores[]){
	for(int i = 0; i< numero; i++){
		if (i == 0){
			trabajadores[i].set_nombre("Benja");
			trabajadores[i].set_sexo("M");
			trabajadores[i].set_edad(20);
		}
		else if (i == 1){
			trabajadores[i].set_nombre("vale");
			trabajadores[i].set_sexo("F");
			trabajadores[i].set_edad(10);
		}
		else if (i == 2){
			trabajadores[i].set_nombre("Nicole");
			trabajadores[i].set_sexo("F");
			trabajadores[i].set_edad(100);
		}
		else if (i == 3){
			trabajadores[i].set_nombre("Marcela");
			trabajadores[i].set_sexo("F");
			trabajadores[i].set_edad(46);
		}
		else if (i == 4){
			trabajadores[i].set_nombre("joaquin");
			trabajadores[i].set_sexo("M");
			trabajadores[i].set_edad(66);
		}
	}
}

/*Función que llama a otras funciones */
void minimain(int promedio, int numero, Trabajador trabajadores[]){
	/* Llamado de funciones */
	registroTrabajadores(numero, trabajadores);
	impresionTrabajador(numero, trabajadores);
	/* se retorna el promedio de las edades de los trabajadores */
	promedio = edadPromedio(numero, trabajadores);
	trabajadorJoven(numero, trabajadores);
	mujerMayorPromedio(promedio, numero, trabajadores);
	hombreMenorPromedio(promedio, numero, trabajadores);
}

/* Main del programa */
int main(){
	int numero = 5;
	/* Variable vacía para guardar el valor que retorna la función */
	int promedio;
	/* Se crea el arreglo de clases */
	Trabajador trabajadores[numero];
	minimain(promedio, numero, trabajadores);
	return 0;
}
