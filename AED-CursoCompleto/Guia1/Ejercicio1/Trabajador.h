#include <iostream>
using namespace std;

#ifndef TRABAJADOR_H
#define TRABAJADOR_H

class Trabajador {
    private:
       string nombre = "\0";
       string sexo = "\0";
       int edad;
       
    public:
        /* constructor */
        Trabajador ();
		void registroTrabajadores();
        /* get y setter nombre*/
        string get_nombre();
        void set_nombre(string nombre);
        /* get y setter sexo */
        string get_sexo();
		void set_sexo(string sexo);
		/* get y setter edad */
		int get_edad();
		void set_edad(int edad);

};
#endif
