#include <fstream>
#include <iostream>
using namespace std;
//~ #include "Nodo.h"
#include "Arbol.h"
/* para usar fork() */
#include <unistd.h>

/*
 * crea el archivo .txt de salida y le agrega
 * contenido del árbol para generar el grafo (imagen).
 */

/*constructor vacío*/
Arbol::Arbol() {}
        
/* 
 * crea un nuevo nodo.
 */
Nodo *crearNodo(int info, Nodo *padre) {
    Nodo *q;
    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->info = info;
    q->padre = padre;
    return q;
}
 
 
 /* ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado es ofstream &nombre_archivo). */
void Arbol::recorrerArbol(Nodo *p, ofstream &archivo) {

  string infoTmp;
  /* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador al final, siendo i: izquierda
   * y d: derecha, esto se cumplirá para los casos en donde los nodos no apunten a ningún otro (nodos finales) 
   * */
  if (p != NULL) {
	/* Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo */  
    if (p->izq != NULL) {
      archivo<< p->info << "->" << p->izq->info << ";" << endl;
    } else {
	  infoTmp = to_string(p->info) + "i";
	  infoTmp = "\"" + infoTmp + "\"";
      archivo << infoTmp << "[shape=point]" << endl;
      archivo << p->info << "->" << infoTmp << ";" << endl;
    }
    
    infoTmp = p->info; 
    if (p->der != NULL) {
      archivo << p->info << "->" << p->der->info << ";" << endl;
    } else {
	  infoTmp = to_string(p->info) + "d";
	   infoTmp = "\"" + infoTmp + "\"";
      archivo << infoTmp << "[shape=point]" << endl;
      archivo << p->info << "->" << infoTmp << ";" << endl;
    }

    /* Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo */
    recorrerArbol(p->izq, archivo);
    recorrerArbol(p->der, archivo); 
  }
  return;
}

void Arbol::crearGrafo(Nodo *raiz) {
    ofstream archivo;  
    /* Se abre/crea el archivo datos.txt, a partir de este se generará el grafo */ 
    archivo.open("datos.txt");
    /* Se escribe dentro del archivo datos.txt "digraph G { " */ 
    archivo << "digraph G {" << endl;
    /* Se pueden cambiar los colores que representarán a los nodos, para el ejemplo el color será verde */
    archivo << "node [style=filled fillcolor=lightblue];" << endl;
    /* Llamado a la función recursiva que genera el archivo de texto para creación del grafo */
    recorrerArbol(raiz, archivo);
    /* Se termina de escribir dentro del archivo datos.txt*/
    archivo << "}" << endl;
    archivo.close();
    
    /* genera el grafo */
    system("dot -Tpng -ografo.png datos.txt &");
    system("eog grafo.png &");
}
 
 
 
 
/* funcion para insertar elementos en el arbol */
void Arbol::insertarNodo(Nodo *&arbol, int n, Nodo *padre){
	if (arbol == NULL){ /* arbol vacío */
		Nodo *nuevoNodo = crearNodo(n, padre);
		arbol = nuevoNodo;
	}
	else { /* si el arbol si tiene nodos */
		
		int valorRaiz = arbol->info; /*se obtiene el valor de la raiz*/
		if (n < valorRaiz){ /* si el elemento es menor que la raioz, go izq*/
			insertarNodo(arbol->izq,n,arbol);
		}
		else if (n > valorRaiz){
			insertarNodo(arbol->der,n,arbol);  /* si el elemento es mayor, insertamos en der*/
		}
		else{ /* elemento igual, por tanto no se inserta */
			cout << "Número no permitido." << endl;
			
		} 
	}
}

/*Ggenera la búsqueda de un dato en específico */
bool Arbol::busqueda(Nodo *arbol, int n){
	if (arbol == NULL){ /*arbol vacio*/
		return false;
	}
	else if (arbol->info == n){ /* si el nodo es iwal al elemento*/
		return true;
	}
	else if (n < arbol->info){ 
		return busqueda(arbol->izq,n);
	}
	else {
		return busqueda(arbol->der,n);
	}
}

/*funcion preOrden profundidad */
void Arbol::pre(Nodo *arbol){
	if (arbol == NULL){ /*arbol vacio */
		return;
	}
	else {
		cout << "["<< arbol->info << "]  ";
		pre(arbol->izq);
		pre(arbol->der);
	}

}

/*funcion recorrido inOrden */
void Arbol::in(Nodo *arbol){
	if (arbol == NULL){
		return;
	}
	else{
		in(arbol->izq);
		cout << "["<< arbol->info << "]  ";
		in(arbol->der);
	}
}

/* post orden */
void Arbol::post(Nodo *arbol){
	if (arbol == NULL){ // si el arbol está vacio 
		return ;
	}
	else {
		post(arbol->izq);
		post(arbol->der);
		cout << "["<< arbol->info << "]  ";
	}
}
/*borrar la existencia de un nodo*/
void Arbol::destruirNodo(Nodo *nodo){
	nodo->izq = NULL;
	nodo->der = NULL;
	delete nodo;
}

// funcion para determinar mas izquierda posible 2 hijos
Nodo *minimo(Nodo *arbol){
	if (arbol == NULL){ /*arbol vacio*/
		return NULL;
	}
	if (arbol->izq) { /*si tiene hijo izq*/
		return minimo(arbol->izq); /*+izq*/
	}
	if (arbol->der){
		return minimo(arbol->izq); /*+izq*/
	}
	else {
		return arbol; /*return mismo nodo*/
	}
}

/* si tiene un solo hijo*/
/*funcion para reemplazar 2 nodos*/
void Arbol::reemplazar(Nodo *arbol, Nodo *nuevo_nodo){
	if (arbol->padre){
		/*arbol->padre hay que darle un nuevo hijo*/
		if (arbol->info == arbol->padre->izq->info){
			arbol->padre->izq = nuevo_nodo;
		}
		else if(arbol->info == arbol->padre->der->info){
			arbol->padre->der = nuevo_nodo;
		}
	}
	
	if (nuevo_nodo){
		/* asignar un nuevo padre*/
		nuevo_nodo->padre = arbol->padre;
	}
}

/*eliminar nodo encontrado*/
void Arbol::eliminarNodo(Nodo *nodoEliminar){
	/*si tiene 2 hijos*/
	if (nodoEliminar->izq && nodoEliminar->der){
		Nodo *menor = minimo(nodoEliminar->der);
		nodoEliminar->info = menor->info;
		eliminarNodo(menor);
	}
	else if (nodoEliminar->izq){ // si tiene hijo izq
		reemplazar(nodoEliminar,nodoEliminar->izq);
		destruirNodo(nodoEliminar);
	}
	else if (nodoEliminar->der){ /*hijo derecho*/
		reemplazar(nodoEliminar, nodoEliminar->der);
		destruirNodo(nodoEliminar);
	}
	else { /*si no tiene hijos*/
		reemplazar(nodoEliminar, NULL);
		//destruirNodo(nodoEliminar);
		
	}
}


/*funcion eliminar nodo */
void Arbol::eliminar(Nodo *arbol, int n){
	if (arbol == NULL){ /*arbol vacio*/
		return; /*nada*/
	}
	else if(n < arbol->info){ // si el valor es menor, busca por la izquierda
		eliminar(arbol->izq,n);
	}
	else if (n >arbol->info){
		eliminar(arbol->der,n);
	}
	else{ /* encontré el elemento */
		eliminarNodo(arbol);
	}
}
