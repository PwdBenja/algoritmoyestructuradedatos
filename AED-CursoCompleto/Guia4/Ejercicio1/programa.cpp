#include <fstream>
#include <iostream>
using namespace std;
#include "Arbol.h"
//~ #include "Grafo.h"
/* para usar fork() */
#include <unistd.h>



/* Función del menú */
void menu(){
	/* árbol */
	Nodo *raiz = NULL;  
    /* objeto de la clase grafo */
    Arbol *g = new Arbol();
    int dato, opcion;
    /* booleando para el while */
    bool aux = true;
	while (aux){
		cout << "  ___________________________________" << endl;
		cout << "  |    ->          MENÚ        <-    |" << endl;
		cout << "  | 1. -> insertar nodo.             |" << endl;
		cout << "  | 2. -> mostrar arbol.             |" << endl;
		cout << "  | 3. -> buscar un nodo.            |" << endl;
		cout << "  | 4. -> Ordenar en pre-orden.      |" << endl;
		cout << "  | 5. -> Ordenar en in-orden.       |" << endl;
		cout << "  | 6. -> Ordenar en post-orden.     |" << endl;
		cout << "  | 7. -> eliminar un nodo.          |" << endl;
		cout << "  | 8. -> Modificar Nodo.                     |" << endl;
		cout << "  | 9. -> salir.                     |" << endl;
		cout << "  |__________________________________|" << endl;
		cin >> opcion;
		/* condiciones */
		switch(opcion){
			case 1: system("clear"); /* Se limpia la terminal */
					cout << "Digite numero: " << endl; 
					cin >> dato;
					g->insertarNodo(raiz,dato,NULL); /* Se llama a insertar Nodo, sin padre asociado */
					
					
					cout << endl;
					break;
			case 2: system("clear"); /* Se limpia la terminal */
					cout << "Mostrando arbol ... " << endl;
					for (int i = 0; i < 3; i++){
						cout << "......." << endl;
					}
					cout << endl;
					/*creaa el grafo*/
					g->crearGrafo(raiz);
					break;
			case 3: system("clear"); /* Se limpia la terminal */
					cout << "digite elemento a buscar: " << endl;
					cin >> dato;
					if (g->busqueda(raiz,dato) == true){
						cout << " elemento encontrado " << endl;
					}
					else {
						cout << " elemento no encontrado " << endl;
					}
					break;
					
			case 4: system("clear"); /* Se limpia la terminal */
					cout << "Recorrido en preOrden: " << endl;
					g->pre(raiz);
					cout << endl;
					break;
			
			case 5: system("clear"); /* Se limpia la terminal */
					cout << "recorrer en inOrden" << endl;
					g->in(raiz);
					cout << endl;
					break;
			case 6: system("clear"); /* Se limpia la terminal */
					cout << "recorrido en postOrden " << endl;
					g->post(raiz);
					cout << endl;
					break;
			case 7: system("clear"); /* Se limpia la terminal */
					cout << "digite dato por eliminar" << endl;
					cin >> dato;
					if (g->busqueda(raiz,dato) == true){
						/* se encontró el elemento, se elimina. */
						g->eliminar(raiz, dato);
					}
					else {
						cout << " elemento no encontrado " << endl;
					}
					cout << endl;
					break;
					
			case 8: system("clear");
					cout << "digite dato por modificar:" << endl;
					cin >> dato;
					g->eliminar(raiz, dato);
					cout << "Ingrese dato nuevo" << endl;
					cin >> dato;
					g->insertarNodo(raiz, dato, NULL);
					break;
			case 9: system("clear");
					aux = false;
		}
	}
}


int main(void) {
	menu();
    return 0;
}
