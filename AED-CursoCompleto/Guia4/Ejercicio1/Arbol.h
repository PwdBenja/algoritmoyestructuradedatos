#include <iostream>
#include <unistd.h>

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

/*estructura del nodo*/
typedef struct _Nodo {
    int info;
    struct _Nodo *izq;
    struct _Nodo *der;
	struct _Nodo *padre;
} Nodo;

class Arbol {
    private:

    public:
        /* constructor*/
		Arbol();
		void recorrerArbol(Nodo *p, ofstream &archivo);
		void crearGrafo(Nodo *raiz);
		void insertarNodo(Nodo *&arbol, int n, Nodo *padre);
		bool busqueda(Nodo *arbol, int n);
		void pre(Nodo *arbol);
		void in(Nodo *arbol);
		void post(Nodo *arbol);
		void destruirNodo(Nodo *nodo);
		void reemplazar(Nodo *arbol, Nodo *nuevo_nodo);
		void eliminarNodo(Nodo *nodoEliminar);
		void eliminar(Nodo *arbol, int n);
};
#endif
