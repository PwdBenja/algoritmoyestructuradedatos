
AL iniciar el programa se visualizará un menú el cual dará las siguientes opciones:

1. La opcion uno, sólo permite ingresar números enteros al árbol.
2. la opcion dos, creará el grafo del árbol.
3. Busca un valor dentro del árbol.
4. Ordena los datos del árbol en pre - orden.
5. Ordena los datos en in-orden.
6. Ordena los datos del arbol en post-orden.
7. Elimina un Nodo.
8. Modificar un nodo.
8. permite salir del programa.

Todo dato ingresado debe ser un valor entero. 
La opción eliminar presenta problemas una vez se haya creado el grafo del árbol.
El programa se compila con ./grafo

Especificaciones técnicas, sólo se pueden eliminar datos que se encuentren a la izquierda, es decir, que pertenezcan a la rama de la izq de la raiz.
Por tanto, tampoco se pueden modificar elementos que no sean del lado izquierdo de la raiz.
Cabe destacar que el primer elemento ingresado, es decir, la raiz no se puede eliminar.

->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

-El programa se compila utilizando el comando make y se ejecuta con ./main
->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
