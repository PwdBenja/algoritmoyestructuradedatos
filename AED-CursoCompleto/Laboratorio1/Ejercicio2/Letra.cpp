#include <iostream>
#include "Letra.h"
#include <string.h>
using namespace std;

/* Constructor */
Letra::Letra(int tamano){	
	this->tamano = tamano;	
}

void Letra::contadorMayusMinus(){
	/* Contadores de mayusculas y minusculas */
	int mayusculas = 0;
	int minusculas= 0;
	/* Longitud del string */
	int longitud = 0;
	/* arreglo char para recorrer la "palabra" ingresada */
	char buffer[longitud];	
	string palabra;
	/* arreglo que almacena las variables string ingresadas por el usuario */
	string arregloPalabras[tamano];
	
	/* Relleno del arregloPalabras */
	for (int z = 0; z < tamano; z++){
		cout << "ingrese palabras: " << endl;
		getline(cin, palabra);
		arregloPalabras[z] = palabra;
	}
	/* Para evitar basura se iguala al valor vacío de string */
	palabra = "\0";
	for (int z = 0; z < tamano; z++){
		palabra = arregloPalabras[z];
		/* Convertir el string en *char */
		strcpy(buffer,palabra.c_str());
		/* Calculo la longitud de la cadena. */
		longitud = strlen(buffer);	
		/* Se recorre la primera palabra del arregloPalabras */
		for (int i = 0; i < longitud; i++){
			/* Condicion minusculas */
			if(buffer[i] >= 'a' and buffer[i]<='z') {
				minusculas++;
			}
			/* Condicion mayusculas */
			else if(buffer[i] >= 'A' and buffer[i]<='Z'){
				mayusculas++;
			}
		}
		/* Se imprimen los valores */
		cout << "La palabra " << arregloPalabras[z] << " tiene:" << minusculas << " minusculas." << endl;
		cout << "La palabra " << arregloPalabras[z] << " tiene:" << mayusculas << " mayusculas." << endl;
		cout << "\n " << " --------- " << endl;
		/* se reestablecen los contadores */
		minusculas = 0;
		mayusculas = 0;
	}	
}
