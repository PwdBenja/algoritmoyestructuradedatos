/* Escriba un programa que lea N frases en un arreglo de caracteres y determine el número de minúsculas
y mayúsculas que hay en cada una de ellas. Puede utilizar las funciones islower() y isupper() de la
librerı́a ctype.h. */

#include "Letra.h"
#include <iostream>
using namespace std;

void procedimiento(){
	int tamano;
	cout << "¿Cuántas palabras ingresará?: " << endl;
	cin >> tamano;
	cin.ignore();
	/* Instancia el objeto de la clase Letra */
	Letra l = Letra(tamano);
	l.contadorMayusMinus();


}

int main (void){
	procedimiento();
	return 0;
}
