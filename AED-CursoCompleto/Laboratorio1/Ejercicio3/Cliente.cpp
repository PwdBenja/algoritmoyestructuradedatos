#include "Cliente.h"
#include <iostream>
using namespace std;

/* constructor */
Cliente::Cliente(){
	
}

/* Get y Set de nombre */
string Cliente::get_nombre() {
    return this->nombre;
}

void Cliente::set_nombre(string nombre) {
    this->nombre = nombre;
}

/* Get y Set de telefono */

string Cliente::get_telefono() {
    return this->telefono;
}

void Cliente::set_telefono(string telefono) {
    this->telefono = telefono;
}
/* Get y Set de saldo */

int Cliente::get_saldo() {
    return this->saldo;
}

void Cliente::set_saldo(int saldo) {
    this->saldo = saldo;
}

/* Get y Set de moroso */

bool Cliente::get_moroso() {
    return this->moroso;
}

void Cliente::set_moroso(bool moroso) {
    this->moroso = moroso;
}
