#include <iostream>
using namespace std;


#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente {
    private:
       string nombre = "\0";
       string telefono = "\0";
       int saldo;
       bool moroso = false;
       
    public:
        /* constructor */
        Cliente ();
        /* get y setter nombre*/
        string get_nombre();
        void set_nombre(string nombre);
        /* get y setter telefono */
        string get_telefono();
		void set_telefono(string telefono);
		/* get y setter saldo */
		int get_saldo();
		void set_saldo(int saldo);
		/* get y setter moroso */
		bool get_moroso();
		void set_moroso(bool moroso);
};
#endif
