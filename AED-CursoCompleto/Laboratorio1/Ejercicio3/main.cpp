#include "Cliente.h"
#include <iostream>
using namespace std;

/* 	Una empresa registra para cada uno de sus N clientes los siguientes datos:
	->Nombre (cadena de caracteres)
	->Teléfono (cadena de caracteres)
	->Saldo (entero)
	->Moroso (booleano)
	Escriba un programa que permita el ingreso de los datos de los clientes y luego permita imprimir sus
	datos indicando su estado (morosos o no). Utilice arreglo de clases en su solución. */

int main(){
	int numClientes;
	cout << "ingrese numero de clientes: " << endl;
	cin >> numClientes;
	/* arreglo objetos */
	Cliente arregloCliente[numClientes];
	/* Variables ayuda */
	string nombre;
	string numero;
	int saldo;
	
	for(int i = 0; i< numClientes; i++){
		cout << "Ingrese nombre cliente: " << endl;
		cin >> nombre;
		cout << "Ingrese numero cliente: " << endl;
		cin >> numero;
		cout << "Ingrese saldo cliente: " << endl;
		cin >> saldo;
		/* Se le asignan los valores a objeto */
		arregloCliente[i].set_nombre(nombre);
		arregloCliente[i].set_telefono(numero);
		arregloCliente[i].set_saldo(saldo);
		/* se revisa la morosidad */
		if (arregloCliente[i].get_saldo() != 0){
			arregloCliente[i].set_moroso(true);
		}
		system("clear");
	}
	system("clear");
	cout << "Los clientes morosos son: " << endl;
	int contador = 0;
	for(int w = 0; w< numClientes; w++){
		if (arregloCliente[w].get_saldo() != 0){
			cout << arregloCliente[w].get_nombre() << endl;
			cout << "\n" ;
			contador++;
		}		
	}
	if (contador == 0){
		cout << "No hay clientes morosos. " << endl;
	}
	return 0;
}
