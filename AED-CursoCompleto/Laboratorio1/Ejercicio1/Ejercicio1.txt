->Ejercicio 1

Al iniciar el programa se definirá el largo del arreglo, el usuario ingresará los valores del arreglo, luego se realizara una operacion que tomará todos los cuadrados de los numeros aleatorios y los sumará, el programa finalizará mostrando el resultado del cálculo realizado. 

-Para compilar se utiliza el comando make luego se ejecuta con ./principal

->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
