#include <iostream>
#include <list>

using namespace std;

#ifndef CUADRADO_H
#define CUADRADO_H

class Cuadrado {
    private:
		/* atributos */
        int numero = 0;
		int *arreglo = NULL;
		int total = 0;
    public:
				
        /* constructor */
        Cuadrado (int numero); 
		void rellenarArreglo();
		void calcularCuadrado();
        /* métodos get */
		int get_total();
};
#endif
