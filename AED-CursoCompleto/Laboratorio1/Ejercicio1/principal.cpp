/* 	Escriba un programa que llene un arreglo unidimensional de números enteros y luego obtenga como
	resultado la suma del cuadrado de los números ingresados. (Considere un arreglo unidimensional de
	tipo entero de N elementos). */

#include "Cuadrado.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;


int main (void){
	srand(time(NULL));
	/* se genera un numero random */
	int random = 1+rand()%(10-1);
	cout << "El tamaño del arreglo es: " << random << endl;
	/* se instancia el objeto de la clase */
	Cuadrado x = Cuadrado(random);
	/* se rellena el arreglo */
	x.rellenarArreglo();
	/* se calcula los cuadrados */
	x.calcularCuadrado();
	/* se obtiene el valor del total de la suma a través de un get*/
	cout << "La suma total de los cuadrados es: " << x.get_total() << endl;
	/* se finaliza el programa */
	return 0;
}
