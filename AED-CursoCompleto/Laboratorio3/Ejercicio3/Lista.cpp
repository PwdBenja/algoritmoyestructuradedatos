#include <iostream>
#include <stdlib.h>
#include "Lista.h"
#include "Postre.h" 
using namespace std;

/* constructor vacío */
Lista::Lista(){}

/*recibe el dato para ingresarlo a la lista, es más fácil que mantener parámetros en el contructor */
void Lista::recibirDato(string dato){
	/*Se iguala el valor recibido de parámetro al atributo */
	this->dato = dato;
	/* Se inserta el valor a la lista */
	insertarLista(this->dato);
}

/* Muestra la lista. */
void Lista::mostrarLista(){
	if (lista != NULL){ /* sólo imprime si la lista no está vacía */
		Nodo *actual = new Nodo(); /* nodo auxiliar */
		actual = lista; /*primer elemento de la lista*/
		/*recorre la lista*/
		while (actual != NULL){ 
			cout << "[" << actual->postre->getNombre() << "]"<< " "; /* se imprimen los postres */ 
			actual = actual->siguiente; /*se mueve posiciones*/
		}
	}
	/*comprueba que la lista está vacía*/
	else {
		cout << "lista vacía " << endl;
		cout << endl;
	}
}

void Lista::imprimirIngredientes(string postreBuscado){
	/*buscar elemento primero antes de llegar e imprimir*/
	bool band = false; /*interruptor */
	Nodo *actual = new Nodo(); /* nodo auxiliar */
	actual = lista; /* actual es el primer elemento de la lista*/
	
	/* buscando coincidencias */
	while(actual != NULL && band == false){
		if(actual->postre->getNombre() == postreBuscado){ /*se hace la busqueda */
			band = true; /*el interruptor se prende */
			actual->postre->mostrarIngrediente(); /* se imprimen los ingredientes del postre encontrado */
		}
		actual = actual->siguiente; /*mueve posiciones dentro de la lista */
	}
	/* si el interruptor nunca se encendió, nunca encontró el postre */
	if (band == false){
		cout << "elemento no encontrado" << endl;
	}
}

/*opcion uno */
/* muesta los ingredientes de un postre en específico */
void Lista::opcionUno(string postreBuscado){
	cout << "Los postres son: " << endl;
	mostrarLista(); /* Muestra los postres, informando al usuario cuales hay */
	cout << endl;
	cout << "Cual postre quiere preparar.?" << endl;
	cin >> postreBuscado;
	system("clear");
	/*imprime ingredientes*/
	imprimirIngredientes(postreBuscado);
	cout << endl;
	menuIngredientes();
}

/*opción dos */
/*agrega ingredientes a un  postre en específico */
void Lista::opcionDos(string postreBuscado){
	cout << "Los postres son: " << endl;
	mostrarLista();
	cout << endl;
	cout << "Cual postre quiere agregarle ingredientes. " << endl;
	cin >> postreBuscado;
	agregarIngredientes(postreBuscado);
	cout << endl;
	menuIngredientes();
}

/*opción tres*/
/*elimina ingredientes de un postre */
void Lista::opcionTres(string postreBuscado){
	if (lista != NULL){  /*comprueba si la lista esta vacía */
		bool band = false; /*interruptor*/
		Nodo *actual = new Nodo(); /* nodo auxiliar */
		actual = lista; /*actual es el primer elemento de la lista */
		string auxiliar = "\0";
		/*Muestra lista de postres,para tener al usuario actualizado*/
		cout << endl;
		cout << "Postres:" << endl;
		mostrarLista();
		cout << endl;
		cout << "de que postre desea eliminar un ingrediente: " << endl;
		cin >> postreBuscado;
		
		/* buscando coincidencias */
		while(actual != NULL && band == false){
			if(actual->postre->getNombre() == postreBuscado){
				band = true; /*se enciende el interruptor*/
				cout << endl;
				cout << "Ingredientes:" << endl;
				actual->postre->mostrarIngrediente();
				cout << "Cuál es el ingrediente que desea eliminar ? si no hay ingredientes, presione cualquier letra" << endl;
				cin >> auxiliar;
				actual->postre->ejecutarEliminado(auxiliar);
			}
			actual = actual->siguiente; /*mueve posiciones dentro de la lista */
		}
		if (band == false){
			cout << "elemento no encontrado" << endl;
		}
	}
}

void Lista::menuIngredientes(){
	/*se comprueba que la lista se encuentre con algún dato previamente ingresado */
	if (lista != NULL){
		string postreBuscado = "\0";
		/*Menú de ingredientes*/
		string opcion = "\0";
		cout << "| Mostrar los ingredientes de un postre ingredientes.| 1 |" << endl;
		cout << "| Agregar los ingredientes de un postre.             | 2 |" << endl;
		cout << "| Eliminar ingrediente de un postre.                 | 3 |" << endl;
		cout << "| salir.                                             | 4 |" << endl;
		cin >> opcion; /*se guarda la selección del usuario */
		if (opcion == "1"){ /* Opción permite visualizar los datos de un Postre en específico */
			system("clear");
			opcionUno(postreBuscado); /*se llama a la opción uno */
		}
		else if (opcion == "2"){ /* Opción que permite agregar ingrediente por ingrediente a un postre en específico*/
			system("clear"); 
			opcionDos(postreBuscado);/*se llama opcion dos*/
		}
		else if (opcion == "3"){ /*Elimina un ingrediente de un postre en específico*/
			opcionTres(postreBuscado);
		}
		else if (opcion == "4"){ /*vuelve al menú principal (postres )*/
			system("clear");
			cout << endl;
		}
		else {
			cout << "Opcion no válida " << endl;
		}
	}
	else {
		/* si la lista se encuentra vacía no ingresará al menú, volviendo al menú principal () de los postres */
		cout << endl;
	}
}

/* agrega elementos a la lista de ingredientes, siempre y cuando
 * el elemento postre exista. */
void Lista::agregarIngredientes(string postreBuscado){
	string z = "\0";
	/* mientras la lista no este vacía */
	if (lista != NULL){
		bool band = false;
		Nodo *actual = new Nodo();
		actual = lista;
		/* buscando coincidencias */
		while(actual != NULL && band == false){
			if(actual->postre->getNombre() == postreBuscado){
				band = true;
				cout << "Ingrese ingrediente agregar: " << endl;
				cin >> z;
				actual->postre->insertarIngrediente(z);
			}
			actual = actual->siguiente; /*mueve posiciones dentro de la lista */
		}
	}
	/*lista vacía*/
	else{
		cout << "lista vacía" << endl;
	}
}

void Lista::ejecutarEliminado(string nombreBuscado){
	/* mientras la lista no este vacía */
	if (lista != NULL){
		/* nodos auxiliares */
		Nodo *aux_borrar;
		Nodo *anterior = NULL;
		/*inicio lista*/
		aux_borrar = lista;
		/*recorre la lista*/
		while (aux_borrar != NULL && aux_borrar->postre->getNombre() != nombreBuscado){
			anterior = aux_borrar;
			aux_borrar = aux_borrar->siguiente; /* se avanza */
		}
		/*elemento buscado no encontrado */
		if (aux_borrar == NULL){
			cout << "El elemento no existe";
		}
		/* el primer elemento se elimina*/
		else if(anterior == NULL){
			lista = lista->siguiente;
			delete aux_borrar;
			cout << "Su lista es: " << endl;
			mostrarLista();
			cout << endl;
		}
		/*el elemento esta en la lista, pero no es el primero uwu*/
		else {
			anterior->siguiente =aux_borrar->siguiente;
			delete aux_borrar;
			cout << "su lista es:" << endl;
			mostrarLista();
			cout << endl;
		}
	}
	/*no se elimina nada, lista vacía*/
	else{
		cout << "lista vacía" << endl;
	}
}

/*insertar elementos a la lista 
 * siempre y cuando no sean iguales. */
void Lista::insertarLista(string n){
	bool band = false;
	Nodo *actual = new Nodo();
	actual = lista;
	
	/* buscando coincidencias */
	while(actual != NULL && band == false){
		if(actual->postre->getNombre() == n){
			band = true;
		}
		actual = actual->siguiente; /*mueve posiciones dentro de la lista */
	}
	/*si el interruptor no se enciende, el postre se agrega a la lista */
	if (band == false){
		Nodo *nuevo_nodo = new Nodo(); /* se reserva memoria al nuevo nodo */
		nuevo_nodo->postre = new Postre(n);
		Nodo *aux1 = lista;
		Nodo *aux2;
		/* Mantiene la lista ordenada
		 * de menor a mayor segun la tabla de ascii  */
		while (aux1 != NULL && aux1->postre->getNombre()[0] < n[0]){
			aux2 = aux1;
			aux1 = aux1->siguiente;
		}
		
		if (lista == aux1){
			lista = nuevo_nodo;
		}
		/* se ha corrido una posición */
		else {
			aux2->siguiente = nuevo_nodo;
		}
		nuevo_nodo->siguiente = aux1;
		system("clear");
		cout << "Postre " << "[" << n << "]" << " añadido a la lista" << endl;
	}
	else {
		system("clear");
		cout << "Ese postre ya existe en nuestra información " << endl;
		cout << endl;
	}
}
