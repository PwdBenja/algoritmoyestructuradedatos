/* POSTRES  uwu*/
/*declaración de librerias */
#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

/* Menú principal (postres)*/
void menu(string dato){
	/* Se le piden los valores al usuario */
	string opcion;
	/* Se instancia el objeto de la clase lista */
	Lista x = Lista();
	/* menú*/
	do{
		cout << endl;
		cout << "| ---------------" << endl;
		cout << "| Ingresar Postres.         | 1 |" << endl;
		cout << "| Eliminar Postre           | 2 |" << endl;
		cout << "| Seleccionar Postre.       | 3 |" << endl;
		cout << "| Mostrar lista de postres. | 4 |" << endl;
		cout << "| Salir.                    | 5 |" << endl;		
		cout << "| ---------------" << endl;
		cin >> opcion;
		/*opciones y condiciones */
		if (opcion == "1"){
			cout << "Digite una palabra: " << endl;
			cin >> dato;
			/*se le pasa el valor string a la lista*/
			x.recibirDato(dato);
		}
		else if (opcion == "2") {
			cout << "ingrese un postre que quiere eliminar" << endl;
			cin >> dato;
			x.ejecutarEliminado(dato);
		}	
		else if (opcion == "3"){
			system("clear");
			x.menuIngredientes();
		}
		else if (opcion == "4"){
			system("clear");
			cout << "Los postres son:" << endl;
			x.mostrarLista();
			cout << endl;
		}
		else if (opcion != "5"){
			cout << "Ingrese otro valor." << endl;
		}
	}while(opcion != "5"); /*condicion del while*/
	
} 
/* main del programa */
int main (){
	/* Puntero lista */
	string dato;
	/* se llama menú */
	menu(dato);
	return 0; 
}
