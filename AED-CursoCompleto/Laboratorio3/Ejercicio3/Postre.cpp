#include <iostream>
#include <stdlib.h>
#include "Postre.h"
using namespace std;

/*se asocia el valor del parámetro al atributo de la clase*/
Postre::Postre(string nombre){
	this->nombre = nombre;
}

/*get de el atributo nombre */
string Postre::getNombre(){
	return this->nombre;
}

void Postre::recibirDato(string dato){
	/*Se iguala el valor recibido de parámetro al atributo */
	this->dato = dato;
	/* Se inserta el valor a la listaIngrediente */
	insertarIngrediente(this->dato);
}

/* Muestra la lista. */
void Postre::mostrarIngrediente(){	
	if (listaIngrediente != NULL){
		Ingrediente *actual = new Ingrediente();
		actual = listaIngrediente; /*primer elemento de la lista*/
		/*recorre la lista*/
		cout << "Ingredientes: " << endl;
		while (actual != NULL){
			cout << "[" << actual->dato << "]"<< " ";
			actual = actual->siguiente; /*se mueve posiciones*/
		}
	}
	else {
		cout << "No se ha ingresado ningún ingrediente a este postre." << endl;
	}
	
}

/*insertar elementos a la listaIngrediente */
void Postre::insertarIngrediente(string n){
	Ingrediente *nuevo_nodo = new Ingrediente(); /* se reserva memoria al nuevo nodo */
	Ingrediente *aux;
	nuevo_nodo->dato = n;
	nuevo_nodo->siguiente = NULL;
	if (listaIngrediente == NULL){ /* listaIngrediente no vacía */
		listaIngrediente = nuevo_nodo; /* se agrega el primer elemento a la listaIngrediente */
	}
	else {
		aux = listaIngrediente;/* aux es el primer elemento */
		while (aux->siguiente != NULL){ /* se recorre la listaIngrediente */
			aux = aux->siguiente; /* avanza posiciones */
		}
		aux->siguiente = nuevo_nodo; /* agrega el nuevo elemento */
	}
	/* Se limpia la pantalla */
	system("clear");
	cout << "elemento " << "[" << n << "]" << " añadido a la listaIngrediente" << endl;
}

void Postre::ejecutarEliminado(string postreBuscado){
	/* mientras la lista no este vacía */
	if (listaIngrediente != NULL){
		/* nodos auxiliares */
		Ingrediente *aux_borrar;
		Ingrediente *anterior = NULL;
		/*inicio lista*/
		aux_borrar = listaIngrediente;
		/*recorre la lista*/
		while (aux_borrar != NULL && aux_borrar->dato != postreBuscado){
			anterior = aux_borrar;
			aux_borrar = aux_borrar->siguiente; /* se avanza */
		}
		/*elemento buscado no encontrado */
		if (aux_borrar == NULL){
			cout << "El elemento no existe";
		}
		/* el primer elemento se elimina*/
		else if(anterior == NULL){
			listaIngrediente = listaIngrediente->siguiente;
			delete aux_borrar;
			mostrarIngrediente();
			cout << endl;
		}
		/*el elemento esta en la lista, pero no es el primero uwu*/
		else {
			anterior->siguiente =aux_borrar->siguiente;
			delete aux_borrar;
			mostrarIngrediente();
			cout << endl;
		}
	}
	/*no se elimina nada, lista vacía*/
	else{
		cout << "lista vacía" << endl;
	}
}
