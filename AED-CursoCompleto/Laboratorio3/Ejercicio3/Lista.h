#include <iostream>
#include <stdlib.h>
#include "Postre.h"
using namespace std;

#ifndef LISTA_H
#define LISTA_H

/*estructura nodo*/
struct Nodo{
	Postre *postre;
	Nodo *siguiente;
};

class Lista {
	/*atributos de la clase*/
    private:
		string dato;
		Nodo *lista = NULL;

    public:
        /* constructor */
		Lista();
		/*recibe el dato como parámetro*/
		void recibirDato(string dato);
		/*imprime la lista*/
		void mostrarLista();
		/*lo ingresa en la lista*/
		void insertarLista(string n);
		/*eliminar un nodo específico */
		void ejecutarEliminado(string valorBuscado);
		/*agregar ingredientes*/
		void agregarIngredientes(string postreBuscado);
		
		/* funciones para manejar los ingredientes */
		/*menú de opciones */
		void menuIngredientes();
		/*opción uno */
		void opcionUno(string postreBuscado); 
		/*opcion dos */
		void opcionDos(string postreBuscado);
		/*opcion tres */
		void opcionTres(string postreBuscado);
		/* imprime los ingredientes de un postre */
		void imprimirIngredientes(string postreBuscado);
};
#endif
