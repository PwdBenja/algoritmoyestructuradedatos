#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef POSTRE_H
#define POSTRE_H


struct Ingrediente{
	string dato;
	Ingrediente *siguiente;
};

class Postre {
    private:
		/*atributos*/
		string dato;
		string nombre;
		Ingrediente *listaIngrediente = NULL;

    public:
        /* constructor */
		Postre(string nombre);
		/*get correspondiente*/
		string getNombre();
		/*recibe el dato como parámetro*/
		void recibirDato(string dato);
		void insertarIngrediente(string dato);
		void mostrarIngrediente();
		void ejecutarEliminado(string postreBuscado);

};
#endif
