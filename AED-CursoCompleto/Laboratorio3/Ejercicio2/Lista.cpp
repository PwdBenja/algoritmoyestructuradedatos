#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;

/* constructor vacío */
Lista::Lista(){}

void Lista::recibirDato(string dato){
	/*Se iguala el valor recibido de parámetro al atributo */
	this->dato = dato;
	/* Se inserta el valor a la lista */
	insertarLista(lista,this->dato);
}

/* Muestra la lista. */
void Lista::mostrarLista(Nodo *lista){	
	Nodo *actual = new Nodo();
	actual = lista; /*primer elemento de la lista*/
	cout << "Su lista es:  " << endl;
	/*recorre la lista*/
	while (actual != NULL){
		cout << "[" << actual->dato << "]"<< " ";
		actual = actual->siguiente; /*se mueve posiciones*/
	}
} 

/*insertar elementos a la lista */
void Lista::insertarLista(Nodo *&lista, string n){
	
	Nodo *nuevo_nodo = new Nodo(); /* se reserva memoria al nuevo nodo */
	Nodo *aux;
	nuevo_nodo->dato = n;
	nuevo_nodo->siguiente = NULL;
	if (lista == NULL){ /* lista no vacía */
		lista = nuevo_nodo; /* se agrega el primer elemento a la lista */
	}
	else {
		aux = lista;/* aux es el primer elemento */
		while (aux->siguiente != NULL){ /* se recorre la lista */
			aux = aux->siguiente; /* avanza posiciones */
		}
		aux->siguiente = nuevo_nodo; /* agrega el nuevo elemento */
	}
	/* Se limpia la pantalla */
	system("clear");
	cout << "elemento " << "[" << n << "]" << " añadido a la lista" << endl;
	mostrarLista(lista);
}
