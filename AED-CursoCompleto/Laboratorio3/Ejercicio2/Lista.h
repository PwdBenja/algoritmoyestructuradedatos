#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

/*estructura nodo*/
struct Nodo{
	string dato;
	Nodo *siguiente;
};

class Lista {
	/*atributos de la clase*/
    private:
		string dato;
		Nodo *lista = NULL;

    public:
        /* constructor */
		Lista();
		/*recibe el dato como parámetro*/
		void recibirDato(string dato);
		/*imprime la lista*/
		void mostrarLista(Nodo *lista);
		/*lo ingresa en la lista*/
		void insertarLista(Nodo *&lista, string n);
};
#endif
