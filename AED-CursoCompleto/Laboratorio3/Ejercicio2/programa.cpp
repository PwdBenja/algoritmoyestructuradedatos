/* Escriba un programa que cree una lista ordenada con nombres (string). Por cada ingreso, muestre el
contenido de la lista. */

#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;
 
void menu(string dato){
	/* Se le piden los valores al usuario */
	string opcion;
	bool zapato = true;
	/* Se instancia el objeto de la clase lista */
	Lista x = Lista();
	/* menú*/
	do{
		cout << endl;
		cout << "---------------" << endl;
		cout << "Insertar elemento. | 1 |" << endl;
		cout << "Salir.             | 2 |" << endl;		
		cout << "---------------" << endl;
		cin >> opcion;
		/*opciones y condiciones */
		if (opcion == "1"){
			cout << "Digite una palabra: " << endl;
			cin >> dato;
			/*se le pasa el valor string a la lista*/
			x.recibirDato(dato);
		}
		else if (opcion == "2"){
			zapato = false;
		}
		else {
			cout << "Ingrese otro valor." << endl;
		}
	}while(zapato == true); /*condicion del while*/
	
} 
 
int main (){
	/* Puntero lista */
	string dato;
	menu(dato);
	return 0;
}
