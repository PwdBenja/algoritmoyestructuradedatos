#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

/* estructura */
struct Nodo{
	int dato;
	Nodo *siguiente;
};

/* clase lista */
class Lista {
	/* atributos de la clase */
    private:
		int dato;
		Nodo *lista = NULL;
		int tamano;

    public:
        /* constructor */
		Lista();
		/* se recibe el dato de main */
		void recibirDato(int dato);
		/* se imprime la lista*/
		void mostrarLista();
		/* se inserta el dato obtenido del main a la lista */
		void insertarLista(Nodo *&lista, int n);
		/* get de los atributos */
		int getTamano();
		int getDato();

};
#endif
