#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

/*  Escriba un programa que cree una lista desordenada de números y luego la divida en dos listas inde-
	pendientes ordenadas ascendentemente, una formada por los números positivos y otra por los números
	negativos. */

/* imprime las listas aunque estas esten vacías */
void imprimirListas(Lista listaDesordenada, Lista listaNegativa, Lista listaPositiva){
	cout << "Lista 1 (Principal):" << endl;
	listaDesordenada.mostrarLista();
	cout << endl;
	cout << "Lista 1 (Negativa):" << endl;
	listaNegativa.mostrarLista();
	cout << endl;
	cout << "Lista 2 (Positivo):" << endl;
	listaPositiva.mostrarLista();
	cout << endl;
}

void menu(int dato){
	/* Se le piden los valores al usuario */
	string opcion;
	bool zapato = true;
	/* Se instancia el objeto de la clase lista */
	Lista listaDesordenada = Lista();
	Lista listaNegativa = Lista();
	Lista listaPositiva = Lista();
	do{
		cout << endl;
		cout << "---------------------------------------------------" << endl;
		cout << "| Insertar elementos en la lista desordenada. | 1 |" << endl;
		cout << "|          Mostrar lista desordenada.         | 2 |" << endl;
		cout << "|            Mostrar lista en dos.            | 3 |" << endl;
		cout << "|                  Salir.                     | 4 |" << endl;
		cout << "---------------------------------------------------" << endl;
		cin >> opcion;
		if (opcion == "1"){
			cout << "Digite un numero: " << endl;
			cin >> dato;
			/* se le agrega el dato a la lista */
			listaDesordenada.recibirDato(dato);
			/* se desplaza el valor del getDato a alguna de las listas */
			int z = listaDesordenada.getDato();
			/* se compara el valor del ultimo dato ingresado a la lista desordenada, se copia y se asigna a una lista */
			if (z >= 0){
				listaPositiva.recibirDato(z);
			}
			/* se rellena la lista negativa */
			if (z < 0){
				listaNegativa.recibirDato(z);
			}
			cout << endl;
		}
		else if (opcion == "2"){
			/* imprime la lista principal */
			cout << "Lista principal: " << endl;
			listaDesordenada.mostrarLista();
			cout << endl;
		}
		else if (opcion == "3"){
			/*Imprime el estado actual de las listas */
			imprimirListas(listaDesordenada,listaNegativa, listaPositiva);
		}
		else if (opcion == "4"){
			zapato = false;
		}
		else {
			cout << "Ingrese otro valor." << endl;
		}
	}while(zapato == true);	
} 
 
int main (){
	/* Puntero lista */
	int dato = 0;
	menu(dato);
	return 0;
}
