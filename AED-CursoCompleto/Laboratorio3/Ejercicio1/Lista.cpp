#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;

/* constructor vacío */
Lista::Lista(){}

void Lista::recibirDato(int dato){
	/*Se iguala el valor recibido de parámetro al atributo */
	this->dato = dato;
	/* Se inserta el valor a la lista */
	insertarLista(lista,this->dato);
}

int Lista::getTamano(){
	return this->tamano;
}
/* get del dato ingresado por el usuario */
int Lista::getDato(){
	return this->dato;
}

/* Muestra la lista. */
void Lista::mostrarLista(){
	/*nodo auxiliar*/	
	Nodo *actual = new Nodo();
	/*actual queda con el primer dato de la lista*/
	actual = lista;
	/* se recorre  y se imprime la lista */
	while (actual != NULL){
		cout << "[" << actual->dato << "]"<< " ";
		actual = actual->siguiente; /* se mueve dentre de la lista*/
	}
} 

/*insertar elementos a la lista */
void Lista::insertarLista(Nodo *&lista, int n){
	
	Nodo *nuevo_nodo = new Nodo(); /* se reserva memoria al nuevo nodo */
	nuevo_nodo->dato = n;
	Nodo *aux1 = lista;
	Nodo *aux2;
	//~ /* Mantiene la lista ordenada
	//~ * de menor a mayor */
	/* revisa si la lista esta vacia */
	while (aux1 != NULL && aux1->dato < n){
		aux2 = aux1;
		aux1 = aux1->siguiente;
	}
	if (lista == aux1){
		lista = nuevo_nodo;
	}
	/* se ha corrido una posición */
	else {
		aux2->siguiente = nuevo_nodo;
	}
	nuevo_nodo->siguiente = aux1;
	/* Se limpia la pantalla */
	system("clear");
	cout << "elemento " << "[" << n << "]" << " añadido a la lista" << endl;
}
