#include <iostream>
#include <list>
#include "Atomo.h"
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
    private:
        string nombre = "\0";
        int numero;
        list<Atomo> atomo;
        
    public:
        /* constructor */
        Aminoacido (string nombre, int numero);
        
        void add_atomo(Atomo atomo);
        /* métodos get */
        string get_nombre();
        int get_numero();
        list<Atomo> get_atomo();
        
        /* métodos set */
        void set_nombre(string nombre);
        void set_numero(int numero);
};
#endif
