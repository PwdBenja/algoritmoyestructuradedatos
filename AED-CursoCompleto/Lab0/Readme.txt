

--> Registro Proteínas.

Empezando:
--> El programa permite el ingreso de una lista de proteínas con su información asociada.

Precauciones:
--> tener cuidado con la entrada de datos, si se solicitan números, solo ingresar números.

Requisitos previos:
--> Sistema operativo Linux.
--> Make
--> g++

Instalación:
--> Para la ejecución del script sólo se debe descargar de https://gitlab.com/Panconjamon/laboratorionumero_0.git.

Constituido por:
--> Ubuntu: Sistema operativo.
--> Make.

Autor:
--> Benjamín Astudillo Alarcón.

Versiones:
--> Ubunto 18.04

Expresiones de gratitud:
--> https://gitlab.com/Panconjamon/laboratorionumero_0.git

