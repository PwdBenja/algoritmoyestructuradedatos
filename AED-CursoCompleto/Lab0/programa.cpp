#include <list>
#include <iostream>
#include "Proteina.h"
#include <stdlib.h>
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

void imprimir_datos_proteinas(list<Proteina> proteina){
	system("clear");
	for (Proteina p: proteina){
        cout << "ID:" << p.get_id() << endl;
		cout << "Nombre: " << p.get_nombre() << endl;
		for (Cadena c: p.get_cadenas()){
			cout << "Cadena:" << c.get_letra() << endl;
			for (Aminoacido a: c.get_aminoacidos()){
				cout << "Amino: " << a.get_nombre() << a.get_numero() << endl;
				for (Atomo o: a.get_atomo()){
					cout << "Atomo: " << o.get_nombre() << o.get_numero() << endl;
					cout << "Coordenada: " << o.get_coordenada().get_x() << " ," << o.get_coordenada().get_y() << " ,"  << o.get_coordenada().get_z() << endl;
					cout << "---------------------" << endl;				
				}
			}
		}				
    }
}

void leer_datos_proteina(){
	list<Proteina> proteina;
	int numero = 1;
	string nombreProteina;
    string nombreCadena;
    string nombreAminoacido;
    string id;
    string letra;
    int numeroAminoacido;
    string nombreAtomo;
    int numeroAtomo; 
    float coordenadaX;
    float coordenadaY;
    float coordenadaZ;
	for (int i = 1; i <= numero; i ++){
		cout << "Ingrese nombre proteina: " << endl;
		cin >> nombreProteina;
		cout << "Ingrese ID proteina: " << endl;
		cin >> id;
		Proteina x = Proteina(nombreProteina, id);
		for (int w = 1; w <= numero; w ++){
			cout << "Ingrese una letra para la cadena:" << endl;
			cin >> letra;
			Cadena u = Cadena(letra);	
			for (int q = 1; q <= numero; q++){
				cout << "Ingrese nombre aminoácido:" << endl;
				cin >> nombreAminoacido;
				cout << "Ingrese numero de aminoácidos: " << endl;
				cin >> numeroAminoacido;
				Aminoacido amino = Aminoacido(nombreAminoacido,numeroAminoacido);
				for (int l = 1; l <= numero; l++){
					cout << "Ingrese nombre del átomo: " << endl;
					cin >> nombreAtomo;
					cout << "Ingrese numero del átomo: " << endl;					
					cin >> numeroAtomo;
					cout << "Coordenada x: " << endl;
					cin >> coordenadaX;
					cout << "Coordenada y: " << endl;
					cin >> coordenadaY;
					cout << "Coordenada z: " << endl;
					cin >> coordenadaZ;
					Coordenada coor = Coordenada(coordenadaX,coordenadaY,coordenadaZ);
					Atomo at = Atomo(nombreAtomo,numeroAtomo);		
					at.set_coordenada(coor);
					amino.add_atomo(at);
					u.add_aminoacidos(amino);
					x.add_cadena(u);
				}
			}
		}
		proteina.push_back(x);
		imprimir_datos_proteinas(proteina);
	}
	
}

int main(void){	
	int numeroProteina;
	cout << "Ingrese canditad de proteinas: " << endl;
	cin>> numeroProteina;
	for (int i = 0; i < numeroProteina; i++){
		leer_datos_proteina();
	}  
	return 0;
}
