#include "Aminoacido.h"
#include <iostream>
#include <list>
#include "Cadena.h"

using namespace std;

Cadena::Cadena (string letra) {    
    this->letra = letra;
}

/* métodos get and set */
string Cadena::get_letra() {
    return this->letra;
}

void Cadena::add_aminoacidos(Aminoacido aminoacido){
	this->aminoacidos.push_back(aminoacido);
}

list<Aminoacido> Cadena::get_aminoacidos(){
	return this->aminoacidos;
}  
      
void Cadena::set_letra(string letra) {
    this->letra = letra;
}
