#include <iostream>
#include <stdlib.h>
#include "Pila.h"
#include <time.h>
using namespace std;


int main(){
	srand(time(NULL));
	string opc;
	int numero = 1+rand()%(5-1);
	Pila x = Pila(numero);
	bool zapatilla = true;
	while (zapatilla){
		/* Menú de opciones */
		cout << "Agregar  [1]" << endl; 
		cout << "Remover  [2]" << endl;
		cout << "Ver pila [3]" << endl;
		cout << "Salir    [0]" << endl;
		cout << "----------------" << endl;
		/* Se le asigna el valor a la variable */
		cin >> opc;
		if (opc == "1"){	
			system("clear"); /* se limpia la terminal */
			/* se llama el método de la clase */
			x.push();
		}
		
		if (opc == "2"){
			system("clear"); /* se limpia la terminal */
			/* se llama el método de la clase */
			x.pop();
		}
		
		if (opc == "3"){
			system("clear"); /* se limpia la terminal */
			/* se llama el método de la clase */
			x.ver();
		}
		
		if (opc == "0"){
			system("clear"); /* se limpia la terminal */
			exit(0); /* se finaliza el programa */
		
		}
	}
	return 0;
} 
