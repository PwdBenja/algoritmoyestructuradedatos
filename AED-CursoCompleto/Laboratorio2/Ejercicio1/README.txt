->Ejercicio 1

Al iniciar el programa se dará de manera random el tamaño de la pila, luego mostrará un menu el cual indicará en la primera opcion que ingrese la cantidad de valores dados por el random, la segunda opcion dará la opcion de remover uno de los numeros de la pila, la tercera opcion mostraŕa la pila, es decirl los numeros de manera ordenada en forma de pila y por ultimo estará la opcion de salir del programa el cual lo cerrará finalizando todo.

-Para compilar se utiliza el comando make luego se ejecuta con ./programa 

->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
