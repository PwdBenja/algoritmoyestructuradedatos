#include <iostream>
#include <stdlib.h>

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
	private:
		/* atributos */
		int maximo = 4;
		int *pila = NULL;
		int tope = 0;
		bool help = true;
		
	public: 
		/* Constructor*/
		Pila (int maximo);
		void push();
		void pop();
		void ver();
		void pilaVacia();
		void pilaLlena();

};
#endif
