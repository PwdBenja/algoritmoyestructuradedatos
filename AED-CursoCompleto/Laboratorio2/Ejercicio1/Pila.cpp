#include <iostream>
#include <stdlib.h>
#include "Pila.h"

using namespace std;

/* constructor vacío */
Pila::Pila(int maximo){
	this->maximo = maximo;
	cout << "La pila tiene un tamaño de: "<< maximo << endl;
	/* el vector pasa a ser un arreglo */
	this->pila = new int [this->maximo];
}

void Pila::pilaVacia(){
	if (tope == 0){
		help = true;
	}
	else{
		help = false;
	}
}

void Pila::pilaLlena(){
	if (tope == maximo){
		help = true;
	}
	else{
		help = false;
	}
}

void Pila::push(){
	pilaLlena();
	if (help == true){
		cout << "Desbordamiento, Pila llena. " << endl;
	}
	else{
		int dato = 1;
		for (int i = 0; i < maximo; i++){
			cout << "[INGRESE]: " << endl;
			cin >> dato;
			pila[tope] = dato;
			tope = tope + 1;
		}
	}
}
void Pila::pop(){
	int dato; 
	pilaVacia();
	if (help == true){
		cout << "Subdesbordamiento, Pila vacı́a. " << endl;
	}
	else {	
		dato = pila[tope-1];
		tope = tope - 1;
		cout << "Eliminado: [" << dato << "]" << endl;
		cout << "\n" << endl;
	} 
}

void Pila::ver(){
	cout << "La pila es: " << endl;
	for (int i = tope-1; i >= 0; i--){
		cout << pila[i] << endl;
	}
	cout << "-------------" << endl;
}
