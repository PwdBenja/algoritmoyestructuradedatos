--> Ejercicio *ARBOLES BALANCEADOS*

El programa contiene un menú el cual trae 5 opciones diversas para realizar una interación con el grafo, dentro de ellas podemos ingresar números, eliminar y modificar nodos y mostrar el grafo, esta opción nos mostrará el árbol de manera gráfica.

-El programa se compila utilizando el comando make y se ejecuta con ./programa

->REQUISITOS PREVIOS

Sistema operativo Linux 
GNU Make (para compilar) 
graphics (para el grafo del árbol.

->CONSTRUIDO CON:

graphics, encargado de construir los grafos.
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.
C++ : lenguaje de programación.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo.

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés y ayudantes. 

