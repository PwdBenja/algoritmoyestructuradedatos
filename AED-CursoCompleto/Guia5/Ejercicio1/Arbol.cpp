#include <fstream>
#include <iostream>
#include "Arbol.h"
/* para usar fork() */
#include <unistd.h>

using namespace std;

// Constructor vacio
Arbol::Arbol(){

}
/* ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado es ofstream &nombre_archivo). */
void Arbol::recorrerArbol(Nodo *p, ofstream &archivo) {
	string infoTmp;
	/* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador*/
	if (p != NULL) {
	// Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo.
	if (p->izq != NULL) {
		archivo<< p->dato << "->" << p->izq->dato << "[label=" << p->factorEquilibrio << "];" << endl;
	}

	else {
		infoTmp = to_string(p->dato) + "i";
		infoTmp = "\"" + infoTmp + "\"";
		archivo << infoTmp << "[shape=point]" << endl;
		archivo << p->dato << "->" << infoTmp << ";" << endl;
	}
	
	infoTmp = p->dato;
	
	if (p->der != NULL) {
		archivo << p->dato << "->" << p->der->dato << "[label=" << p->factorEquilibrio << "];" << endl;
	}

	else {
		infoTmp = to_string(p->dato) + "d";
		infoTmp = "\"" + infoTmp + "\"";
		archivo << infoTmp << "[shape=point]" << endl;
		archivo << p->dato << "->" << infoTmp << ";" << endl;
	}
	
	// Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo.
	recorrerArbol(p->izq, archivo);
	recorrerArbol(p->der, archivo);
	}

	return;
}


void Arbol::crearGrafo(Nodo *raiz) {
    ofstream archivo;  
    /* Se abre/crea el archivo datos.txt, a partir de este se generará el grafo */ 
    archivo.open("datos.txt");
    /* Se escribe dentro del archivo datos.txt "digraph G { " */ 
    archivo << "digraph G {" << endl;
    /* Se pueden cambiar los colores que representarán a los nodos, para el ejemplo el color será verde */
    archivo << "node [style=filled fillcolor=purple];" << endl;
    /* Llamado a la función recursiva que genera el archivo de texto para creación del grafo */
    recorrerArbol(raiz, archivo);
    /* Se termina de escribir dentro del archivo datos.txt*/
    archivo << "}" << endl;
    archivo.close();
    
    /* genera el grafo */
    system("dot -Tpng -ografo.png datos.txt &");
    system("eog grafo.png &");
}

/* Crea un nuevo nodo. */
Nodo* Arbol::crearNodo(int infor, Nodo *padre){
    Nodo *otro;

    otro = new Nodo;  

    /* Asigna un valor al nodo */
    otro->dato = infor;

    /* Se crean los subarboles izquierdo y derecho */
    otro->izq = NULL;
    otro->der = NULL;
    otro->nodo_padre = padre;
    
    return otro;
}
/* Rotación de las ramas derecha */
void Arbol::rotacionDD(Nodo *&raiz, bool bo, Nodo *nodo){
    raiz->der = nodo->izq;
    nodo->izq = raiz; 
    
    /* Actualiza el factor de cambio de los nodos modificados */
    if(nodo->factorEquilibrio == 0){
        raiz->factorEquilibrio = 1;
        nodo->factorEquilibrio = -1;
        bo = false;
    }
    
    else if(nodo->factorEquilibrio == 1){
        raiz->factorEquilibrio = 0;
        nodo->factorEquilibrio = 0;
    }
    
    /* Se asigna un nuevo valor a la raiz */
    raiz = nodo;
}

/* Rotación derecha y luego izquierda */
void Arbol::rotacionDI(Nodo *&raiz, Nodo *nodo, Nodo *nodo1){
    nodo1 = nodo->izq;
    raiz->der = nodo1->izq;
    nodo1->izq = raiz; 
    nodo->izq = nodo1->der;
    nodo1->der = nodo;

    if(nodo1->factorEquilibrio == 1){
        raiz->factorEquilibrio = -1;
    }

    else{
        raiz->factorEquilibrio = 0;
    }

    if(nodo1->factorEquilibrio == -1){
        nodo->factorEquilibrio = 1;
    }

    else{
        nodo->factorEquilibrio = 0;
    }
    
    raiz = nodo1;
    nodo1->factorEquilibrio = 0;
}

void Arbol::reestructuracionIzq(bool bo, Nodo *&raiz){
    Nodo *nodo;
    Nodo *nodo1;

    if(bo == true){
        /* Verifica el valor del factor de equilibrio */
        if(raiz->factorEquilibrio == -1){
            raiz->factorEquilibrio = 0;
        }

        else if(raiz->factorEquilibrio == 0){
            raiz->factorEquilibrio = 1;
            bo = false;
        }

        else if(raiz->factorEquilibrio == 1){
            nodo = raiz->der;
            /* Identifica la rotación */
            if(nodo->factorEquilibrio >= 0){
               rotacionDD(raiz, bo, nodo);
            }

            else{
                rotacionDI(raiz, nodo, nodo1);
            }
        }
    }
}

/* Rotación izquierda */
void Arbol::rotacion_II(Nodo *&raiz, bool bo, Nodo *nodo){
    raiz->izq = nodo->der;
    nodo->der = raiz; 
    
    if(nodo->factorEquilibrio == 0){
        raiz->factorEquilibrio = -1;
        nodo->factorEquilibrio = 1;
        bo = false;
    }
    
    else if(nodo->factorEquilibrio == -1){
        raiz->factorEquilibrio = 0;
        nodo->factorEquilibrio = 0;
    }

    /* Se asigna un nuevo valor a la raiz. */
    raiz = nodo;
}

/* Rotación izquierda y luego derecha */
void Arbol::rotacion_ID(Nodo *&raiz, Nodo *nodo, Nodo *nodo1){
    nodo1 = nodo->der;
    raiz->izq = nodo1->der;
    nodo1->der = raiz; 
    nodo->der = nodo1->izq;
    nodo1->izq = nodo;

    if(nodo1->factorEquilibrio == -1){
        raiz->factorEquilibrio = 1;
    }

    else{
        raiz->factorEquilibrio = 0;
    }

    if(nodo1->factorEquilibrio == 1){
        nodo->factorEquilibrio = -1;
    }

    else{
        nodo->factorEquilibrio = 0;
    }
    
    raiz = nodo1;
    nodo1->factorEquilibrio = 0;
}

void Arbol::reestructuracionDer(bool bo, Nodo *&raiz){
    Nodo *nodo;
    Nodo *nodo1;
    
    if(bo == true){
        /* Verifica el valor del factor de equilibrio  */
        if(raiz->factorEquilibrio == 1){
            raiz->factorEquilibrio = 0;
        }

        else if(raiz->factorEquilibrio == 0){
            raiz->factorEquilibrio = -1;
            bo = false;
        }

        else if(raiz->factorEquilibrio == -1){
            nodo = raiz->izq;
            /* Identifica la rotación */
            if(nodo->factorEquilibrio <= 0){
                rotacion_II(raiz, bo, nodo);
            }

            else{
                rotacion_ID(raiz, nodo, nodo1);
            }
        }
    }
}

/* Inserta el nodo */
void Arbol::insertar(Nodo *&raiz, int infor, bool &bo, Nodo *padre){
    int valor_raiz;

    if(raiz == NULL){
        Nodo *otro;
        otro = crearNodo(infor, padre);
        raiz = otro;
        insertar(raiz, infor, bo, padre);
    }
    
    else{
        valor_raiz = raiz->dato;
        
        /* Inserta nodos en el subárbol izquierdo */
        if(infor < valor_raiz){
            /* Llamada recursiva para agregar nuevos nodos */ 
            insertar(raiz->izq, infor, bo, raiz);
            reestructuracionDer(bo, raiz);
            
        }
        
        /* Inserta nodos en el subárbol derecho */
        else if(infor > valor_raiz){
            /* Llamada recursiva para agregar nuevos nodos */
            insertar(raiz->der, infor, bo, raiz);
            reestructuracionIzq(bo, raiz);
        }
       
        /* Muestra que el número ya se encuentra en el árbol */
        else{
            cout << infor << " se encuentra en el árbol";
        }
    }
    bo = true;
}

/* Función encargada de eliminar el nodo */
void Arbol::eliminar(Nodo *&raiz, bool &bo, int infor){
    Nodo *otro;
    Nodo *aux1;
    Nodo *aux2;
    bool bandera;

    if(raiz != NULL){
        /* Se identifica el subárbol al que pertenece el dato */
        if(infor < raiz->dato){
            /* Llamada recursiva para eliminar el número */
            eliminar(raiz->izq, bo, infor);
            /* Reestruturación de los elementos restantes */
            reestructuracionIzq(bo, raiz);
        }
        
        else if(infor > raiz->dato){
            eliminar(raiz->der, bo, infor);
            reestructuracionDer(bo, raiz);
        }
        
        else{
            otro = raiz;
            
            /* Subárbol derecho vacío */
            if(otro->der == NULL){
                raiz = otro->izq;
            }
            
            else{
                /* Subárbol izquierdo vacío */
                if(otro->izq == NULL){
                    raiz = otro->der;
                }
                /* Elementos con dos descendientes */
                else{ 
                    aux1 = raiz->izq;
                    bandera = false;
                   
                    /* Se sustituye por el nodo más a la derecha */
                    while(aux1->der != NULL){
                        aux2 = aux1;
                        aux1 = aux1->der;
                        bandera = true;
                    }

                    /* se cambia dato por raiz */
                    raiz->dato = aux1->dato;
                    otro = aux1;
                    
                    if(bandera == true){
                        aux2->der = aux1->izq;
                    }
                    
                    else{
                        raiz->izq = aux1->izq;
                        free(otro); /* se limpia la memoria. */
                    }
                }
            }
        }
    }
    
    else{
        cout << "La información no se encuentra en el árbol";
    }
}
