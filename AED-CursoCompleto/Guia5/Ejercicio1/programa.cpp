#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include "Arbol.h"

using namespace std;

/* Función encargada del ingreso de valores asociados al nodo, en el respectivo árbol */
int ingresoInfor(){
    int infor;
    system("clear");
    cout << "\nDigite un número: ";
    cin >> infor;
    /* retorna el valor ingresado. */
    return infor;
}

void ingreso(Arbol *arbol, Nodo *&raiz, bool &bo){
    Nodo *nodo = NULL;
    /* Se pide el número del nodo */
    int infor = ingresoInfor();

    /* Primer nodo del árbol. */
    nodo = arbol->crearNodo(infor, NULL);

    /* Se inicializa la raiz del árbol. */
    if(raiz == NULL){
        raiz = nodo;
    }

    /*Si el árbol ya tiene nodos, entra aquí. */           
    else{
        arbol->insertar(raiz, infor, bo, NULL);
    }
}

/* Función " menú ", encargada de controlar el programa. */
void menu(){
	/* Se crea el objeto de la clase. */
    Arbol *arbol = new Arbol();
    Nodo *raiz = NULL;
    int opcion; 
    bool bo = false;
    
	/* ciclo while. */
    while(opcion != 5){ 
		
		cout << "->>> Árbol Balanceado" << endl;
        cout << "___________________________________"<< endl;
        cout << "|                                 |"<< endl;
        cout << "|   --->         MENÚ         <---|"<< endl;
        cout << "|1. ---> Ingresar número          |"<< endl;
        cout << "|2. ---> Eliminar nodo            |"<< endl;
        cout << "|3. ---> Modificar nodo           |"<< endl;
        cout << "|4. ---> Mostrar árbol            |"<< endl;
        cout << "|5. ---> Salir                    |"<< endl;
        cout << "|_________________________________|"<< endl;
        cin >> opcion;
        /* casos posibles. */
        switch(opcion){
            case 1:
                /* Crea un nodo y se le asocia un dato. */
                ingreso(arbol, raiz, bo);
                break;

            case 2:
                /* A partir del infor deseado se elimina su respectivo Nodo. */
                arbol->eliminar(raiz, bo, ingresoInfor());
                break;

            case 3: 
                /* Se elimina el Nodo deseado y se agrega otro nodo, con su respectivo 
                 * número asociado. */
                arbol->eliminar(raiz, bo, ingresoInfor());
                cout << "---> Número nuevo: ";
                ingreso(arbol, raiz, bo);
                break;

            case 4:
                /* Se muestra la forma del árbol actual. */
                arbol->crearGrafo(raiz);
                break;
        }
        system("clear");
    }
}
/* Se crea el main del programa. */
int main(void){
	/* Se llama a la función menú*/
    menu();
    /* se retorna 0, debido a que es un vacío */
    return 0;
}
