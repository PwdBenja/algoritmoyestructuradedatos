#include <iostream>
#include <unistd.h>
#include <iostream>

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

/*
 * Altura rama derecha = AD
 * Altura rama izquierda = AI
 * Factor equilibrio = FE
 * */

/* Estructura del nodo */
typedef struct _Nodo {
    int dato;
    int factorEquilibrio; /* FE = (AD - AI) */

    struct _Nodo *izq;
    struct _Nodo *der;
    struct _Nodo *nodo_padre;
} Nodo;

class Arbol{
    private:

    public:
		/* constuctor*/
        Arbol();
		void crearGrafo(Nodo *raiz);
        void recorrerArbol(Nodo *p, ofstream &archivo);

        Nodo* crearNodo(int numero, Nodo *padre);
        void insertar(Nodo *&raiz, int numero, bool &bo, Nodo *padre);
        void eliminar(Nodo *&raiz, bool &bo, int numero);

        void reestructuracionIzq(bool bo, Nodo *&raiz);
        /* Rotaciones reestructuración izquierda */         
        void rotacionDD(Nodo *&raiz, bool bo, Nodo *nodo);
        void rotacionDI(Nodo *&raiz, Nodo *nodo, Nodo *nodo1);

        void reestructuracionDer(bool bo, Nodo *&raiz);
        /* Rotaciones reestructuración derecha */
        void rotacion_II(Nodo *&raiz, bool bo, Nodo *nodo);
        void rotacion_ID(Nodo *&raiz, Nodo *nodo, Nodo *nodo1);
};
#endif
