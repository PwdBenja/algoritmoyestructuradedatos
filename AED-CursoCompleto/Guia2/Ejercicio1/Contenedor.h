#include <iostream>
#include <stdlib.h>

using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor {
	private:
		/* atributos */
		string nombre = "\0";
		string id = "\0";
		
	public: 
		/* Constructor*/
		Contenedor();
		string get_nombre();
		void set_nombre(string nombre);
		string get_id();
		void set_id(string id); 
		

};
#endif
