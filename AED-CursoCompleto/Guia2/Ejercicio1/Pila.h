#include <iostream>
#include <stdlib.h>
#include "Contenedor.h"
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
	private:
		/* atributos */
		int maximo;
		Contenedor *pila = NULL;
		int tope = 0;
		bool help = true;
		
	public: 
		/* Constructor*/
		Pila ();
		void tamanoArreglo(int maximo);
		void push();
		void pop();
		void ver(int k);
		void pilaVacia();
		void pilaLlena();
		int get_tope();

};
#endif
