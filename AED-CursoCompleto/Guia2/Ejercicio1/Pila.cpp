#include <iostream>
#include <stdlib.h>
#include "Pila.h"
#include "Contenedor.h"
using namespace std;

/* constructor vacío */
Pila::Pila(){
}

void Pila::tamanoArreglo(int maximo){
	this->maximo = maximo;
	this->pila = new Contenedor [this->maximo];

}

int Pila::get_tope(){
	return this->tope;
}

void Pila::pilaVacia(){
	if (tope == 0){
		help = true;
	}
	else{
		help = false;
	}
}

void Pila::pilaLlena(){
	if (tope == maximo){
		help = true;
	}
	else{
		help = false;
	}
}

void Pila::push(){
	pilaLlena();
	if (help == true){
		cout << "Desbordamiento, Pila llena. " << endl;
	}
	else{
		string datoIngresado;
		Contenedor dato;
			cout << "[INGRESE NOMBRE]: " << endl;
			cin >> datoIngresado;
			pila[tope].set_nombre(datoIngresado);
			cout << "[INGRESE ID]: " << endl;
			cin >> datoIngresado;
			pila[tope].set_id(datoIngresado);
			tope = tope + 1;
	}
}

void Pila::pop(){
	Contenedor dato; 
	pilaVacia();
	if (help == true){
		cout << "Subdesbordamiento, Pila vacı́a. " << endl;
	}
	else {	
		
		dato = pila[tope-1];
		//pila[tope-1].set_id("");
		pila[tope-1].set_nombre("");
		tope = tope - 1;
		cout << "Eliminado: [" << dato.get_nombre() << "]" << endl;
		cout << "\n" << endl;
	} 
}

void Pila::ver(int k){
	
	cout << pila[k].get_nombre();
}
