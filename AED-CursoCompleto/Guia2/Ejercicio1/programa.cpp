#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "Contenedor.h"
#include "Pila.h"
using namespace std;


/* Se transforma el vector tipo Contenedor a un arreglo tipo Contenedor, se le asigna un tamaño a dicho arreglo en esta función. */
void darTamanoArreglo(int numero, Pila arregloPila[]){
	cout << "Cada pila tiene una cantidad de contenedores de: " << numero << endl;
	for (int i = 0; i < numero; i ++){
		arregloPila[i].tamanoArreglo(numero);
	}
}

void agregarEmpresa(int numero, Pila arregloPila[]){
	int k ;/* --------> Pila donde se agregará el contenedor*/
	cout << "Hay " << numero << " pilas" << endl;	
	cout << "Ingrese la pila donde desea agregar la empresa: " << endl;
	cin >> k;
	arregloPila[k-1].push();
}

void removerEmpresa(int numero, Pila arregloPila[]){
	int k ;/* --------> Pila donde se agregará el contenedor*/
	cout << "Hay " << numero << " pilas" << endl;	
	cout << "Ingrese la pila que desea eliminar: " << endl;
	cin >> k;
	arregloPila[k-1].pop();
}

void visualizacion(int numero, Pila arregloPila[]){

	cout << "La pila es: " << endl;
	cout << endl;
	for (int i = numero-1; i >= 0; i --){
		for (int k = 0; k < numero; k++){
			arregloPila[k].ver(i) ;
			cout << "\t|";
		}
		cout << endl;
	}
}

void menu(int numero){
	string opc;
	bool zapatilla = true;
	Pila arregloPila[numero];
	/* Se inicializan los arreglos tipo Contenedor */
	darTamanoArreglo(numero, arregloPila);
	while (zapatilla){		
		/* Menú de opciones */
		cout << "Agregar  [1]" << endl; 
		cout << "Remover  [2]" << endl;
		cout << "Ver pila [3]" << endl;
		cout << "Salir    [0]" << endl;
		cout << "----------------" << endl;
		
		/* Se le asigna el valor a la variable */
		cin >> opc;
		if (opc == "1"){	
			system("clear"); /* se limpia la terminal */
			agregarEmpresa(numero, arregloPila);
		}
		
		if (opc == "2"){
			system("clear"); /* se limpia la terminal */
			removerEmpresa(numero, arregloPila);
		}
		
		if (opc == "3"){
			system("clear"); /* se limpia la terminal */
			visualizacion(numero, arregloPila);
		
		}
		
		if (opc == "0"){
			system("clear"); /* se limpia la terminal */
			exit(0); /* se finaliza el programa */		
		}
	}
}

int main(int argc, char *argv[]){
	srand(time(NULL));
	int numero = stoi(argv[1]);
	menu(numero);
	return 0;
} 
