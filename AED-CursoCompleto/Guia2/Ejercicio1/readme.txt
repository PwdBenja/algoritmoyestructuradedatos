

->ejecución: El programa se deberá ejecutar con un parámetro el cual represemtará la cantidad de pilas y el tamaño de la pila ( n y m respectivamente).
Una vez dentro habrá un menú que dispondrá de varias acciones, como agregar, eliminar, visualizar los elementos de la pila. 


->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

