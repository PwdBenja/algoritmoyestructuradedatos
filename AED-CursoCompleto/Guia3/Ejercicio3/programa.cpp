/* 
 *  Considere que tiene una lista enlazada simple de número enteros, ordenados crecientemente (Lea los
	datos). Donde pueden existir valores no correlativos (ejemplo: 10 - 11 - 15 - 16 - 20). Escriba un
	programa que complete la lista, de tal manera que la misma, una vez modificada almacene todos los
	valores a partir del número del primer nodo hasta el último número del último nodo. Para el ejemplo,
	la lista guardará los números: 10 - 11 - 12 - 13 - 14 - 15 - 16 - 17 - 18 - 19 - 20. */


#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;
 
/* Se elimina y rellana nuevamente */
void rellenar(Lista x, int dato){
	/* Se obtiene el valor más pequeño y más grande de la lista */
	int mayor = x.getMayor();
	int menor = x.getMenor();
	/* Se vacía la lista, permitiendo rellenarla denuevo*/
	x.ejecutarEliminado();	
	cout << "rellenando ..." << endl;
	/*se rellena la lista desde el valor más pqueño hasta el más grande */ 
	for (int i = menor; i <= mayor; i++){
		x.recibirDato(i);
	}
	
	cout << "La lista rellenada es: " << endl;
	/* Se muestra la nueva lista */
	x.mostrarLista();
}
 
void menu(int dato){
	/* Se le piden los valores al usuario */
	string opcion;
	bool zapato = true;
	/* Se instancia el objeto de la clase lista */
	Lista x = Lista();
	do{
		cout << endl;
		cout << "---------------" << endl;
		cout << "Insertar elemento. | 1 |" << endl;
		cout << "Rellenar lista.    | 2 |" << endl;
		cout << "Salir.             | 3 |" << endl;
		cin >> opcion;
		cout << "---------------" << endl;
		if (opcion == "1"){
			cout << "Digite un numero: " << endl;
			cin >> dato;
			x.recibirDato(dato);
		}
		else if (opcion == "2"){
			cout << "Ordenando...." << endl;
			cout << "Su lista ordenada de menor a mayor es: " << endl;
			x.mostrarLista();
			x.calcularMayor();
			rellenar(x, dato);
		}
		else if (opcion == "3"){
			zapato = false;
		}
		else {
			cout << "Ingrese otro valor." << endl;
		}
	}while(zapato == true);
	
} 
 
int main (){
	int dato = 0;
	menu(dato);
	return 0;
}
