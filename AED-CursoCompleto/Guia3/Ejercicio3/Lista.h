#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H


struct Nodo{
	int dato;
	Nodo *siguiente;
};

class Lista {
    private:
		int dato;
		Nodo *lista = NULL;
		int mayor = 0;
		int menor = 99;

    public:
        /* constructor */
		Lista();
		void ejecutarEliminado();
		void eliminar();
		void calcularMayor();
		void recibirDato(int dato);
		void mostrarLista();
		void insertarLista(Nodo *&lista, int n);
		int getMayor();
		int getMenor();
};
#endif
