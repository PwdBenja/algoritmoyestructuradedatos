#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;

/* constructor vacío */
Lista::Lista(){}

/*recibe el dato desde programa.cpp y lo inserta en la lista */
void Lista::recibirDato(int dato){
	/*Se iguala el valor recibido de parámetro al atributo */
	this->dato = dato;
	/* Se inserta el valor a la lista */
	insertarLista(lista,this->dato);
}

/* Muestra la lista. */
void Lista::mostrarLista(){	
	Nodo *actual = new Nodo(); /* nodo auxiliar */
	actual = lista;
	cout << "Su lista es:  " << endl;
	while (actual != NULL){
		cout << "[" << actual->dato << "]"<< " ";
		actual = actual->siguiente; /* se mueve una posición en la lista */
	}
} 

/* get de los atributos */

int Lista::getMayor(){
	return this->mayor;
}

int Lista::getMenor(){
	return this->menor;
}

/* vaciar lista */
void Lista::eliminar(){
	Nodo *aux = lista;
	lista = aux->siguiente;
	delete aux;
} 

void Lista::ejecutarEliminado(){
	while (lista != NULL){ /* mientras no sea el final, seguirá eliminando */
		eliminar();
	}
}

void Lista::calcularMayor(){
	while (lista!=NULL){ /* recorrer la lista */
		if(lista->dato > mayor){ /* Buscando el dato mayor */
			this->mayor = lista->dato;
		}
		if (lista->dato < menor){ /* Buscando el dato menor */
			this->menor = lista->dato;
		}
		lista = lista->siguiente; /* se avanza una posición en la lista */
	}
}

/*insertar elementos a la lista 
 * se inserta un nuevo nodo */
void Lista::insertarLista(Nodo *&lista, int n){
	Nodo *nuevo_nodo = new Nodo(); /* se reserva memoria al nuevo nodo */
	nuevo_nodo->dato = n;
	Nodo *aux1 = lista;
	Nodo *aux2;
	/* Mantiene la lista ordenada
	 * de menor a mayor */
	while (aux1 != NULL && aux1->dato < n){
		aux2 = aux1;
		aux1 = aux1->siguiente;
	}
	
	if (lista == aux1){
		lista = nuevo_nodo;
	}
	/* se ha corrido una posición */
	else {
		aux2->siguiente = nuevo_nodo;
	}
	nuevo_nodo->siguiente = aux1;
	/* Se limpia la pantalla */
	system("clear");
	cout << "elemento " << "[" << n << "]" << " añadido a la lista" << endl;
}
