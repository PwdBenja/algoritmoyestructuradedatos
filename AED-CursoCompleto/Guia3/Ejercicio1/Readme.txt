->Ejercicio 1
El programa se compila utilizando el comando make y se ejecuta con ./programa 
Al ejecutarlo el programa dispondrá un menú sencillo el cual tiene dos opciones, agregar elementos a la lista y salir del programa.
El programa facilitará el ingreso de NUMEROS ENTEROS, los cuales se ordenaran de manera ordenada (menor a mayor) en la lista.
Cada vez que se ingrese un valor nuevo a la lista, esta se imprimirá así actualizando los datos al usuario.

->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

-El programa se compila utilizando el comando make y se ejecuta con ./main
->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
