#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;

/* constructor vacío */
Lista::Lista(){}

void Lista::recibirDato(int dato){
	/*Se iguala el valor recibido de parámetro al atributo */
	this->dato = dato;
	/* Se inserta el valor a la lista */
	insertarLista(lista,this->dato);
}

/* Muestra la lista. */
void Lista::mostrarLista(Nodo *lista){	
	Nodo *actual = new Nodo();
	actual = lista;
	cout << "Su lista es:  " << endl;
	while (actual != NULL){
		cout << "[" << actual->dato << "]"<< " ";
		actual = actual->siguiente;
	}
} 

/*insertar elementos a la lista */
void Lista::insertarLista(Nodo *&lista, int n){
	
	Nodo *nuevo_nodo = new Nodo(); /* se reserva memoria al nuevo nodo */
	nuevo_nodo->dato = n;
	Nodo *aux1 = lista;
	Nodo *aux2;
	/* Mantiene la lista ordenada
	 * de menor a mayor */
	while (aux1 != NULL && aux1->dato < n){
		aux2 = aux1;
		aux1 = aux1->siguiente;
	}
	
	if (lista == aux1){
		lista = nuevo_nodo;
	}
	/* se ha corrido una posición */
	else {
		aux2->siguiente = nuevo_nodo;
	}
	nuevo_nodo->siguiente = aux1;
	/* Se limpia la pantalla */
	system("clear");
	cout << "elemento " << "[" << n << "]" << " añadido a la lista" << endl;
	mostrarLista(lista);
}
