/* Escriba un programa que solicite números enteros y los vaya ingresando a una lista enlazada simple
ordenada. Por cada ingreso debe mostrar el estado de la lista */

#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;
 
void menu(int dato){
	/* Se le piden los valores al usuario */
	string opcion;
	bool zapato = true;
	/* Se instancia el objeto de la clase lista */
	Lista x = Lista();
	do{
		cout << endl;
		cout << "---------------" << endl;
		cout << "Insertar elemento. | 1 |" << endl;
		cout << "Salir.             | 2 |" << endl;
		cin >> opcion;
		cout << "---------------" << endl;
		if (opcion == "1"){
			cout << "Digite un numero: " << endl;
			cin >> dato;
			x.recibirDato(dato);
		}
		else if (opcion == "2"){
			zapato = false;
		}
		else {
			cout << "Ingrese otro valor." << endl;
		}
	}while(zapato == true);
	
} 
 
int main (){
	/* Puntero lista */
	int dato = 0;
	menu(dato);
	return 0;
}
