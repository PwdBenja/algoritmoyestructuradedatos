#include <iostream>
#include <stdlib.h>
using namespace std;

#ifndef LISTA_H
#define LISTA_H


struct Nodo{
	int dato;
	Nodo *siguiente;
};

class Lista {
    private:
		int dato;
		Nodo *lista = NULL;
		int tamano = 0;
    public:
        /* constructor */
		Lista();
		void contarElementos();
		void recibirDato(int dato);
		void mostrarLista(); // Nodo *lista
		void insertarLista(Nodo *&lista, int n);
		int getTamano();
		int getDato();
};
#endif
