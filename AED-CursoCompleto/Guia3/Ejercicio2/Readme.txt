->Ejercicio 2

El ejercicio 2 se ejecuta con make y por terminal ./programa
Al ejecutar el programa aparecerá un menú que dispondrá de cuatro opciones las cuales son:

1. La opción uno es ingresar valores a una primera lista que tiene por nombre lista1, donde cada vez que se le ingrese un dato 
se le imprimirá la lista actualizada al usuario.

2. La opción dos es idéntica a la opción 1, con la diferencia del nombre de la lista, la cual se llama lista2.

3. La opcion tres es mostrar la lista 3, conformada con los datos de la lista 1 y lista 2.

4. La opcion cuatro es salir del programa.

Cabe destacar que cada lista se encuentra ordenada de menor a mayor y que son únicamente para valores enteros.

->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

-El programa se compila utilizando el comando make y se ejecuta con ./main
->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.
