/*
Escriba un programa que genere dos listas enlazadas ordenadas (lea los datos) y forme una tercera
lista que resulte de la mezcla de los elementos de ambas listas. Muestre el contenido finalmente de las
tres listas.
* 
*/
#include <iostream>
#include <stdlib.h>
#include "Lista.h"
using namespace std;

void menu(int dato){
	int x;
	string opcion;
	bool zapato = true;
	/* Se instancia los objetos de la clase lista */
	Lista lista1 = Lista();
	Lista lista2 = Lista();
	Lista lista3 = Lista();
	do{
		cout << endl;
		cout << "---------------" << endl;
		cout << "Insertar elemento lista 1. | 1 |" << endl;
		cout << "Insertar elemento lista 2. | 2 |" << endl;
		cout << "Mostrar lista 3            | 3 |" << endl;
		cout << "Salir.                     | 4 |" << endl;
		cin >> opcion;
		cout << "---------------" << endl;
		if (opcion == "1"){
			/* Se limpia la pantalla */
			system("clear");
			cout << "Digite un numero: " << endl;
			cin >> dato;
			lista1.recibirDato(dato);
			lista1.contarElementos();
			cout << "elemento " << "[" << lista1.getDato() << "]" << " añadido a la lista" << endl;
			lista1.mostrarLista();
			x = lista1.getDato();
			lista3.recibirDato(x);
		}
		else if (opcion == "2"){
			/* Se limpia la pantalla */
			system("clear");
			cout << "Digite un numero: " << endl;
			cin >> dato;
			lista2.recibirDato(dato);
			lista2.contarElementos();
			cout << "elemento " << "[" << lista2.getDato() << "]" << " añadido a la lista" << endl;
			lista2.mostrarLista();
			x = lista2.getDato();
			lista3.recibirDato(x);
		}
		else if (opcion == "3"){
			cout << "generando lista 3" << endl;
			lista3.mostrarLista();
		}
		else if (opcion == "4"){
			zapato = false;
		}
		else {
			cout << "Ingrese otro valor." << endl;
		}
	}while(zapato == true);
} 
 
int main (){
	int dato = 0;
	menu(dato);
	return 0;
}
