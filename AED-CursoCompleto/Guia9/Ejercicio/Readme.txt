- GUIA 9
El programa se ejecuta con ./main pero requiere de un parámetro. El parámetro vendría siendo el método para tratar la colisión, que son:
-> L o l
-> C o c
-> D o d
-> E o e
entonces el programa se compila de la siguiente forma: ./main metodoRespectivo}
Una vez ejecutado el programa, lo primero es pedirle que ingrese el tamaño del arreglo. Una vez ingresado el tamaño, el menú aparecerá.
El menú consiste de 3 opciones:
La primera opción es de ingresar valores al arreglo.
La segunda opción es de buscar un elemento en el arreglo.
y la última opción es salir del programa.

Especificaciones:
EL PROGRAMA SÓLAMENTE Y UNICAMENTE FUNCIONA CON NÚMEROS, PORFAVOR NO INGRESAR LETRAS.
EL PROGRAMA TIENE UN ERROR EL CUAL POR ALGUNA RAZON EN ALGUNOS COMPUTADORES NO IMPRIME LA SOLUCIÓN DEL MÉTODO UTILIZADO, SI HA USTED NO LE
IMPRIME LA SOLUCIÓN DEL MÉTODO, PORFAVOR PRUEBE CON OTRO COMPUTADOR.

->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.
make

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

https://gitlab.com/Panconjamon, Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Material subido a la plataforma de educandus por Alejandro Valdés.

