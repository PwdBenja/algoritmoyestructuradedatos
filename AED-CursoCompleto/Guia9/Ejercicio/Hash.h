#include <iostream>
using namespace std;

#ifndef HASH_H
#define HASH_H

typedef struct _nodo{ // Estructura del nodo
	int numero;
	struct _nodo *sig;
} nodo;


class Hash {
    private:

    public:
		/* constructor */
		Hash ();
		void imprimirColision(int* array, int x);
		void colisionesPruebaLineal(int posicion, int* array, int x, int aux);
		void colisionesPruebaCuadratica(int posicion, int* array, int x, int aux);
		void colisionesDobleDireccion(int posicion, int* array, int x, int aux);
		/* Ingreso */
		void colisionesPruebaLinealIngreso(int posicion, int* array, int x, int aux);
		void colisionesPruebaCuadraticaIngreso(int posicion, int* array, int x, int aux);
		void colisionesDobleDireccionIngreso(int posicion, int* array, int x, int aux);
		/* Encadenamiento */
		void copiarArreglo(nodo **arreglo1, int* array, int x);
		void imprimirLista(nodo **arreglo, int posicion);
		void metodoEncadenamiento(int* array, int x, int aux, int posicion);



};
#endif
