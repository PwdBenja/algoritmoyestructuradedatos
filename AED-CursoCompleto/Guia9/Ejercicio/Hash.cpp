#include "Hash.h"
#include <iostream>

using namespace std;

/* Constructor vacío */
Hash::Hash(){
	
}

/* Inicio Colisión de Busqueda */
void Hash::imprimirColision(int* array, int x){
	/* Recorre el array y imprime la posición y el valor. */
    for(int i = 0; i < x; i++){
        cout << "[" <<  i << "]"  " [" << array[i] << "]"<< endl;
    }
}

void Hash::colisionesPruebaLineal(int posicion, int* array, int x, int aux){
    int nuevaPosicion;
    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << "[" <<  aux << "]"  " [Se encuentró en la posición] [" << posicion << "]"<< endl;
    }
	else{
    	nuevaPosicion = posicion + 1;
        while(array[nuevaPosicion] != '\0' && nuevaPosicion <= x && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = nuevaPosicion + 1;
            /* Vuelve a empezar*/
            if(nuevaPosicion >= x){
                nuevaPosicion = 0;
            }
        }
        if(array[nuevaPosicion] == '\0' || (nuevaPosicion == posicion)){
            cout << "[" <<  aux << "]" "[No se encuentró dentro del array]" << endl;
            imprimirColision(array, x);
        }
	    else{
            cout << "[" <<  aux << "]" " [Se encuentró en la posición] [" << nuevaPosicion << "]"<< endl;
            imprimirColision(array, x);
        }
    }
}

void Hash::colisionesPruebaCuadratica(int posicion, int* array, int x, int aux){
    int i;
    int nuevaPosicion;
    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << aux << "[Se encuentró en la posición] [" << posicion << "]"<< endl;
    }
    else{
        i = 1;
        nuevaPosicion = (posicion + (i * i)); 
        /* Determina si la nueva posición esta vacia */
        while(array[nuevaPosicion] != '\0' && array[nuevaPosicion] != aux){ 
            /* Avanza posiciones */
            i = i + 1; 
            /* Genera una nueva posición */
            nuevaPosicion = (posicion + (i * i)); 

            if(nuevaPosicion > x){ 
                i = 1;
                /* Se reinicia la posición partiendo de nuevo. */
                nuevaPosicion = 0;
                posicion = 0;
            }
        }
        if(array[nuevaPosicion] == '\0'){
            cout << "[" <<  aux << "]" << "[No existe dentro del array] " << endl;
            imprimirColision(array, x);
        }

	    else{
            cout << "[" <<  aux << "] [se encuentra en la posición] [" << nuevaPosicion << "]" << endl;
            imprimirColision(array, x);
        }
    }
}

void Hash::colisionesDobleDireccion(int posicion, int* array, int x, int aux){
    int nuevaPosicion;
    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << "[" <<  aux << "]" "[Se encontró en la posición] " << posicion << endl;
    }
    else{
        nuevaPosicion = ((posicion + 1)%x - 1) + 1;
        while(nuevaPosicion <= x && array[nuevaPosicion] != '\0' && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = ((nuevaPosicion + 1)%x -1) + 1; 
        }

        if(array[nuevaPosicion] == '\0' || (nuevaPosicion == posicion)){ 
            cout << "[" <<  aux << "]" "[No se encuentró]" << endl;
            imprimirColision(array, x);
        }

	    else{
            cout << "[" <<  aux << "]" "[Se encuentró]" << nuevaPosicion << endl;
            imprimirColision(array, x);
        }
    }
}

void Hash::colisionesPruebaLinealIngreso(int posicion, int* array, int x, int aux){
    int nuevaPosicion;
    int contador;
    if(array[posicion] != '\0' && array[posicion] == aux){ 
         cout << "[" <<  aux << "]" "[Se encuentró en la posición][ " << posicion << "]" << endl;
    }
    else{
        contador = 0;
        nuevaPosicion = posicion + 1; 
        while(array[nuevaPosicion] != '\0' && nuevaPosicion <= x && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = nuevaPosicion + 1;
            if(nuevaPosicion >= x){
                nuevaPosicion = 0; 
            }
            contador = contador + 1; 
        }
		/*Si el contador es igual al tamaño del arreglo, no se encuentran 
		 * espacios vacíos dentro del arreglo. */
        if(contador == x){ 
            cout << "[Sin espacio del ARREGLO]" << endl;
        }
        if(array[nuevaPosicion] == '\0'){
            array[nuevaPosicion] = aux;
            cout << "[" << aux << "] [Se movió a la posición] [" << nuevaPosicion << "]\n" << endl;
            imprimirColision(array, x);
        }
    }
}
 
void Hash::colisionesPruebaCuadraticaIngreso(int posicion, int* array, int x, int aux){
    int i;
    int nuevaPosicion;
	/* Determina si el número se encuentra dentro del arreglo. */
    if(array[posicion] != '\0' && array[posicion] == aux){ 
		cout << "[" << aux << "]" "[Se encuentró en el espacio] [" << posicion << "]" << endl;
    }
    else{
        i = 1;
        /* Avanza posiciones cuadráticamente. */
        nuevaPosicion = (posicion + (i * i)); 
        /* Determina si la posición está vacía. */
        while(array[nuevaPosicion] != '\0' && array[nuevaPosicion] != aux){ 
            /* Avanza en las posiciones. */
            i = i + 1;
            /* Genera una nueva posición */
            nuevaPosicion = (posicion + (i * i)); 
            if(nuevaPosicion > x){ 
                i = 1;
                /* Vuelve a la posición 0 del arreglo. */
                nuevaPosicion = 0; 
                posicion = 0;
            }
        }
        /* El número se movió, y se le avisa al usuario donde quedo. */
        if(array[nuevaPosicion] == '\0'){
            array[nuevaPosicion] = aux;
            cout << aux << "]  [se movió a la] [" << nuevaPosicion << "] [Posición]" << "\n" << endl;
            imprimirColision(array, x);
        }
    }
}

/* R */
void Hash::colisionesDobleDireccionIngreso(int posicion, int* array, int x, int aux){ 
    int nuevaPosicion;
    /* Si se encuentra el número se avisa al usuario. */
    if(array[posicion] != '\0' && array[posicion] == aux){ 
        cout << "[" << aux << "] [Se encontró en la posición]: [" << posicion << "]" << endl;
    }
    else{
        nuevaPosicion = ((posicion + 1)%x - 1) + 1; 
        while(nuevaPosicion <= x && array[nuevaPosicion] != '\0' && nuevaPosicion != posicion && array[nuevaPosicion] != aux){
            nuevaPosicion = ((nuevaPosicion + 1)%x - 1) + 1;
        }
        /* Si la nueva posición esta vacía */
        if(array[nuevaPosicion] == '\0'){ 
            array[nuevaPosicion] = aux;
            if(nuevaPosicion == x + 1){
                cout << "[Sin espacio en el arreglo]" << endl;
            }
            else{
                cout << aux << "]  [se movió a la] [" << nuevaPosicion << "] [Posición]" << "\n" << endl;
                imprimirColision(array, x);
            }
        }
    }
}
/* Fin colisiones de ingreso */

/* Encadenamiento */
void Hash::copiarArreglo(nodo **arreglo1, int* array, int x){
    for(int i = 0; i < x; i++){
	    arreglo1[i]->numero = array[i];
        arreglo1[i]->sig = NULL;
    }
}

void Hash::imprimirLista(nodo **arreglo, int posicion){
    nodo *aux = NULL;
    aux = arreglo[posicion];
	while(aux != NULL){
        if(aux->numero != 0){
		    cout << "[" << aux->numero << "] - " << endl;
        }
		aux = aux->sig;
    }
}

void Hash::metodoEncadenamiento(int* array, int x, int aux, int posicion){
    nodo *temporal = NULL;
    nodo *arregloAux[x];
    for(int i = 0; i < x; i++){
        arregloAux[i] = new nodo();
		arregloAux[i]->sig = NULL;
		arregloAux[i]->numero = '\0';
    }
    copiarArreglo(arregloAux, array, x);
    if(array[posicion] != '\0' && array[posicion] == aux){
        cout << "[" << aux << "" << "] [En la posición]: [" << posicion << "]" << endl;
    }
    else{
	    temporal = arregloAux[posicion]->sig;
        while(temporal != NULL && temporal->numero != aux){
            temporal = temporal->sig;
        }
        if(temporal == NULL){
            cout << "[" << aux << "] [No está en la lista] " << endl;
            cout << "[Arreglo]: "  << endl;
	        imprimirLista(arregloAux, posicion);
        }
		else{
    		cout << "[" << aux << "] [En posición] [" << posicion << "]" << endl;
            cout << "\n[Arreglo]: "  << endl;
	        imprimirLista(arregloAux, posicion);
        }
    } 
}
/* Fin encadenamiento */
