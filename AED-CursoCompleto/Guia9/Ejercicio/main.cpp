#include <time.h>
#include <iostream>
#include "Hash.h"

using namespace std;

/* Función que se encarga de rellenar el arreglo, de imprimirlo, etc.*/
int Arreglo(int* array, int x, int flag){
	int contador = 0;
	if(flag == 1){
		cout << "[Su arreglo es]:" << endl;
	}
	/* Se recorre el arreglo */
	for(int i = 0; i < x; i++){
		if(flag == 0){
			/* Se rellena de manera que en cada posición 
			* este el valor "NULL" por defecto. */
			array[i] = '\0';
		}
		if(flag == 1){
			/* Se imprime el contenido del arreglo. */
			cout << " [" << array[i] << "]";
		}
		/* Flag con un valor 2 se encarga de revisar si se encuentran 
		 * elementos en el arreglo. */
		if(flag == 2 && array[i] != '\0'){
			contador = contador + 1;
		}
	}
	if(flag == 2){
		if(contador > 0){
			/*Si el arreglo si se encuentra con elementos en su 
			 * interior el contador se encontrará con un valor
			 *  diferente 0, y por tanto, retornara un TRUE*/ 
			return -1;
		}
		else{ 
			/* Si el contador no suma, y por tanto es 0  */
			return 10000;
		}
	}
	/* Saltos de línea para efectos visuales */
	cout << endl;
	cout << endl;
	cout << endl;
	/* uwu */
	return 0;
}

/* Se pide por pantalla. */
int ingresoValores(int flag){
	int x;
	system("clear");
	cout << "----------------------------------------" << endl;
	cout << endl;
	if(flag == 0){
		cout << "[Ingrese el tamaño del arreglo]:" << endl;
	}
	else if(flag == 1){
		cout << "[Ingrese número]: " << endl;
	}
	/* Se le asocia el valor a la variable */
	cin >> x;
	/*FALTAVALIDACIONSOLONUMERO*/
	/*FALTAVALIDACIONSOLONUMERO*/
	/* Se retorna el valor ingresado. */
	return x;
}
/* ################################################*/

void ColisionesInsercion(int* array, int x, int aux, string metodoColision, int posicion, Hash hash){
    if(metodoColision.compare("L") == 0){
		hash.colisionesPruebaLinealIngreso(posicion, array, x, aux);
    }

    else if(metodoColision.compare("C") == 0){
		hash.colisionesPruebaCuadraticaIngreso(posicion, array, x, aux);
    }

    else if(metodoColision.compare("D") == 0){
		hash.colisionesDobleDireccionIngreso(posicion, array, x, aux);
    }

    else if(metodoColision.compare("E") == 0 ){
        hash.metodoEncadenamiento(array, x, aux, posicion);
    }  
}
/* ################################################ */

void ColisionesBusqueda(int* array, int x, int aux, string metodoColision, int posicion, Hash hash){
    if(metodoColision.compare("L") == 0){
        hash.colisionesPruebaLineal(posicion, array, x, aux);
    }

    else if(metodoColision.compare("C") == 0){
        hash.colisionesPruebaCuadratica(posicion, array, x, aux);
    }

    else if(metodoColision.compare("D") == 0){
        hash.colisionesDobleDireccion(posicion, array, x, aux);
    }

    else if(metodoColision.compare("E") == 0 ){
        hash.metodoEncadenamiento(array, x, aux, posicion);
    }
}

/* Determinara la posición donde se agregara el dato */
void fHash(int* array, int x, int aux, string metodoColision, Hash hash){ 
    int posicion;
    /* Asigna posición para guardar el dato */
    posicion = (aux%(x - 1)) + 1;
    /* Existe Colisión */ 
    if(array[posicion] != '\0'){
        cout << "[Colision en posición]: " << posicion << endl;
        /* Se revisa el método a utilizar */
        ColisionesInsercion(array, x, aux, metodoColision, posicion, hash); 
        Arreglo(array, x, 1);
    }
	/* Si no existe colisión, se agrega. */
    else{
        array[posicion] = aux;
        /* La función arreglo, se le avisa con el parámetro 1, que
         * debe imprimir su contenido.  */
        Arreglo(array, x, 1);
    }
}

/*Función encargada de controlar el programa e interactuar con el 
 * usuario. */
void menu(int* array, int x, string metodoColision){
	/* Se crea y se instancia el objeto de la única clase del programa. */
	Hash hash = Hash();
	/* Variable que se encarga de buscar la posición, donde 
	 * se encontrará el valor buscado. */
	int posicion;
	/* Se declara variable para usarse dentro de la interacción
	 * con el usuario. */
	int opcion;
	/* Variable auxiliar, que ayudará en las diferentes interacciones 
	 * con el usuario. */
	int aux = 0, aux2 = 0;
	/* Variable booleana, para la condicional del While. */
	bool bandera = true;
	/* Limpia la terminal al ejecutar. */
	system("clear");
	/* Se llama la función para rellenar el arreglo. 
	 * el parámetro 0, le avisa a la función que lo rellene. */
	Arreglo(array, x, 0);
	/* Se le pasan x parámetros el arreglo y su
	 * respectivo tamaño. */
	/* Se imprime el menú para interactuar con el usuario. */
	while(bandera){
		cout << " [1] --> [Insertar Numero al Arreglo] <--- " << endl;
		cout << " [2] --> [ Buscar Número al Arreglo ] <--- " << endl;
		cout << " [0] --> [           Salir          ] <--- " << endl;
		cout << endl;
		/* Se pide por pantalla el valor de la opción
		 * y se le asigna a la variable "OPCION". */
		cin >> opcion;
		/* Si el usuario ingresó un número entra al SWITCH a comparar. */
		/* Casos posibles de ingresar
		 * con la variable "OPCION" a comparar. */			
		switch(opcion){
			case 1:
				/* Se asocia el valor return de la función a aux. */
				aux = ingresoValores(1);
				/* Se llama la función "FHASH", donde se supervisa
				 * si existe o no colisión. */
				fHash(array, x, aux, metodoColision, hash);
				/* Se le pasan 3 parámetros, el arreglo y su respectivo
				 * tamaño, además del método de colisión a usar. */
				break;
			case 2:
				/* Se revisa si el arreglo se encuentra lleno. */
				aux2 = Arreglo(array, x, 2);
				if(aux2 == -1){
					/* Se pide en número. */
                    aux = ingresoValores(1);
                    /* Se le asocia el número a la variable y se define
                     * la posición donde se buscará el dato. */ 
                    posicion = (aux%(x - 1)) + 1;
					/* Si el dato coincide con la posición */
                    if(array[posicion] == aux){
                        cout << "[" << aux << "] [En la posición] [" << posicion << "]" << endl;
                        Arreglo(array, x, 1);
                    }
					/* Si no coincide, se revisa con que 
					 * método se resolver la colisión*/
                    else{
                        cout << "[Colisión posición] [" << posicion << "]" << endl;
                        ColisionesBusqueda(array, x, aux, metodoColision, posicion, hash); // 
                    }
                }
                /* Si el arreglo no tiene elementos, está vacío. */
                else{
                    cout << "[Arreglo vacío] [:c]" << endl;
                }
                /* Fin comparación 2 */
				break;
				
			case 0:
				/* Se limpia la pantalla, se finaliza el programa.*/
				system("clear");
				cout << "[Se finalizó el programa]. " << endl;
				/* La bandera pasa false, para no volver al while. */
				bandera = false;
				/* Se finaliza el while, y se finaliza el programa. */
				exit(1);
				break;
			/* Si no es ninguna de las comparaciones. */	
			default:
				/* No se encontró un caso posible.*/
				cout << "[Valór ingresado no válido]" << endl;
				break;
		}	
	}	
}

/* Se encargará de avisarle al usuario que el método 
 * ingresado existe o no. */
void revisionMetodo(string metodoColision){
	/* Convierte el parámetro ingresado en minúsculas a mayúsculas. */
	metodoColision[0] = toupper(metodoColision[0]);
	/* Se revisa si el método existe. */
    if(metodoColision.compare("L") == 0 || metodoColision.compare("C") == 0){
		cout << "[Método válido]" << endl;
    }
    else if(metodoColision.compare("D") == 0 || metodoColision.compare("E") == 0){
		cout << "[Método válido]" << endl;
    }
    else{
        system("clear");
        cout << "[Parámetro Inválido]" << endl;
        cout << "[Revise el README para más información] ... "<< endl;
        /* Se finaliza el programa, debido a parámetros inválidos. */
        exit(1);
    }   
}

/* Se crea el main. */
int main(int argc, char *argv[]){
	/* Se crea la variable para almacenar el tipo de método 
	 * para la colisión. */
	string metodoColision = "\0";
	/* Asignación de valores a las variables. */
	if(argc == 2){
		/* Se asigna el tipo de método ingresado por el usuario. */
		metodoColision = argv[1] ;
		revisionMetodo(metodoColision);
	}
	/* Validación de parámetros. */
	else{
		cout << "[Parámetros inválidos]" << endl;
		return 1;
	}
	/*tamaño del arreglo. */
	int x = 0;
	/* Se crea el arreglo. */
	int* array;
	/* Se pide el tamaño del arreglo al usuario. */
	x = ingresoValores(0);
	cout << x << endl;
	if (x > 0){
		/* Se instancia el arreglo */
		array = new int[x];
	}
	/* Función "MENU", la cual, controla el programa e interactúa
	 * con el usuario. */
	menu(array, x, metodoColision);	
}
