#include "Numero.h"
#include <time.h>
#include <iostream>

using namespace std;

/* imprime todos los datos del array */
void imprimirNumeros(Numero* array, int x){
	for(int i = 0; i< x; i++){
		cout <<	"array["<< i << "] = " << array[i].get_valor() << " ";
	}
	cout << endl;
}


/* Inicio Quicsort*/
int divideQuicksort(Numero* array, int start, int end){
    int left;
    int right;
    Numero pivot;
    Numero temp;
 
    pivot = array[start];
    left = start;
    right = end;
 
    while (left < right) {
        while (array[right].get_valor() > pivot.get_valor()) {
            right--;
        }
 
        while ((left < right) && (array[left].get_valor() <= pivot.get_valor())) {
            left++;
        }
        if (left < right) {
            temp = array[left];
            array[left] = array[right];
            array[right] = temp;
        }
    }
    temp = array[right];
    array[right] = array[start];
    array[start] = temp;
    return right;
}

void quicksort(Numero* array, int start, int end){

    int pivot;
 
    if (start < end) {
        pivot = divideQuicksort(array, start, end);
        
        quicksort(array, start, pivot - 1);

        quicksort(array, pivot + 1, end);
    }
}
/*  Fin Quicksort*/


/*Método de shell*/
void shell(Numero* array, int x, int verConfirmacion){
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	int entero;
	int i;
	Numero aux;
	bool band;
	entero = x + 1;
	while (entero > 1){
		entero = (int)(entero/2);
		band = true;
		while (band == true){
			band = false;
			i = 0;
			while ((i + entero) < x){
				if (array[i].get_valor() > array[(i + entero)].get_valor()){
					aux = array[i];
					array[i] = array[(i + entero)];
					array[(i + entero)] = aux;
					band = true;
				}
				i = i + 1;
			}
		}	
	}
	if(verConfirmacion == 0){
		tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
		cout << "[Shell]:             " << tiempo << " milisegundos" << endl;
	}
	else {
		cout << "[Shell]:             ";
		imprimirNumeros(array, x);
	}
}

/*Método de selección*/
void seleccion(Numero* array, int x, int verConfirmacion){
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	Numero menor;
	int k;
	for (int i = 0; i < x - 1; i++){
		menor = array[i];
		k = i;
		for (int j = i + 1; j < x; j ++){
			if (array[j].get_valor() < menor.get_valor()){
				menor = array[j];
				k = j;
			}
		}
		array[k] = array[i];
		array[i] = menor;
		
	}
	if(verConfirmacion == 0){
		tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
		cout << "[Selección]:         " << tiempo << " milisegundos" << endl;
	}
	else {
		cout << "[Selección]:         ";
		imprimirNumeros(array, x);
	}
}

/*Método inserción Binaria. */
void insercionBinaria(Numero* array, int x, int verConfirmacion){
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	Numero aux;
	int izq;
	int der;
	int m;
	int j;
	int i;
	for (i = 1; i < x; i++){
		aux = array[i];
		izq = 0;
		der = (i - 1);
		while (izq <= der){
			m = (int)((der + izq)/2);
			if (aux.get_valor() < array[m].get_valor()){
				der = m - 1;
			}
			else {
				izq = m + 1;
			}
		}
		j = i - 1;
		while (j >= izq){
			array[j + 1] = array[j];
			j = j - 1;
		}
		array[izq] = aux;
	}
	if (verConfirmacion == 0){
		tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
		cout << "[Inserción Binaria]: " << tiempo << " milisegundos" << endl;
	}
	else {
		cout << "[Incersion Binaria]: ";
		imprimirNumeros(array, x);
	}
	
	
}

/*Método inserción. */
void insercion(Numero* array, int x, int verConfirmacion){
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	Numero aux;
	int k;
	for (int i = 0; i < x; i ++){
		aux = array[i];
		k = (i - 1);
		while ((k >= 0) && (aux.get_valor() < array[k].get_valor())){
			array[k + 1] = array[k];
			k = (k - 1);
		}
		array[k + 1] = aux;
	}
	if (verConfirmacion == 0){
		tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
		cout << "[Inserción]:         " << tiempo << " milisegundos" << endl;
	}
	else {
		/* imprime el array ordenado por Inserción. */
		cout << "[Inserción]:         ";
		imprimirNumeros(array, x);
	}
}

/*Método Burbuja mayor. */
void burbujaMayor(Numero* array, int x, int verConfirmacion){
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	Numero aux;
	aux.set_valor(0);
	for (int i = 0; i < x; i ++){
		for (int j = 0; j < x - 1; j ++){
			if (array[j].get_valor() > array[j + 1].get_valor()){
				aux = array[j];
				array[j] = array[j + 1];
				array[j + 1] = aux;
			}
		}
	}
	if (verConfirmacion == 0){
		tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
		cout << "[Burbuja Mayor]:     " << tiempo << " milisegundos" << endl;	
	}
	else {
		/* imprime el array ordenado por Burbuja mayor. */
		cout << "[Burbuja Mayor]:     ";
		imprimirNumeros(array, x);
	}
}

/*Método Burbuja menor. */
void burbujaMenor(Numero* array, int x, int verConfirmacion){
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();

	Numero aux;
	for (int i = 0; i < x; i ++){
		for (int j = x - 1; j > i; j --){
			if (array[j - 1].get_valor() > array[j].get_valor()){
				aux = array[j - 1];
				array[j - 1] = array[j];
				array[j] = aux;
			}
		}
	}
	if(verConfirmacion == 0){
		tiempo = (clock() - comienzo) / (double)CLOCKS_PER_SEC * 1000;
		cout << "[Burbuja Menor]:     " << tiempo << " milisegundos" << endl; 
	}
	else {
		/* imprime el array ordenado por Burbuja menor. */
		cout << "[Burbuja Menor]:     ";
		imprimirNumeros(array, x);
	}
}

/* Función que registra los números */
void registroNumero(Numero* array, int x, int verConfirmacion){
	/* Rellena el array de forma aleatoria. */
	for(int i = 0; i < x ; i ++){
			array[i].set_valor(1 + rand() % 10);
	}
	/*imprime el array recién rellenado */
	cout << "[Números]:           ";
	imprimirNumeros(array, x);
	cout << endl;

}

/*Esta función se encarga de entregarle los mismos numeros a cada método de ordenamientos*/
void copiarArreglo(Numero* array, int x, int verConfirmacion){
	
	/*Se crea un arreglo para cada método*/
	Numero* array1 = new Numero[x];
	Numero* array2 = new Numero[x];
	Numero* array3 = new Numero[x];
	Numero* array4 = new Numero[x];
	Numero* array5 = new Numero[x];
	Numero* array6 = new Numero[x];
	Numero* array7 = new Numero[x];
	/* Se le asignan los mismos números */
	array1 = array;
	array2 = array;
	array3 = array;
	array4 = array;
	array5 = array;
	array6 = array;
	array7 = array;
	
	/* Métodos de ordenamiento */
	burbujaMenor(array1, x, verConfirmacion);
	burbujaMayor(array2, x, verConfirmacion);
	insercion(array3, x, verConfirmacion);
	insercionBinaria(array4, x, verConfirmacion);
	seleccion(array5, x, verConfirmacion);
	shell(array6, x, verConfirmacion);
	
	/*Impresiones Quicksort*/
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	quicksort(array7, 0, x - 1);
	if(verConfirmacion == 0){
		tiempo = (clock() - comienzo) / (double)CLOCKS_PER_SEC * 1000;
		cout << "[Quicksort]:         " << tiempo << " milisegundos" << endl; 
	}
	else {
		cout << "[Quicksort]:         ";
		imprimirNumeros(array, x);
	}
}

/*Función encargada de llamar los métodos de ordenamientos */
void control(Numero* array, int x, int verConfirmacion){
	cout << "[Métodos de Ordenamientos]  " << endl;
	cout << endl;
	/* llamado de funciones */
	/* Rellena el arreglo aleatoriamente */
	registroNumero(array, x, verConfirmacion);
	copiarArreglo(array, x, verConfirmacion);
}

/* Se crea el main. */
int main(int argc, char *argv[]){
	/* Validación de erróres.*/	
	/*Verifica la entrada de parámetros, si no hay se finaliza.*/
	if(argv[0] == NULL or argv[1] == NULL or argv[2] == 0){
		cout<< "[No Ingresó Parámetros]" << endl;
		return 0;
	}
	try{
		char n = stoi(argv[1]);
	}
	catch (const std::invalid_argument& e){
		cout << "[El primer parámetro debe ser un número]" << endl;
		exit(1);
	}
	/*Fin validación de erróres. */
	
	/*Números random*/
	srand(time(0));
	/*tamaño del arreglo*/
	int x = stoi(argv[1]);
	string ver = argv[2];
	int verConfirmacion;
	/* Se crea el arreglo de clases */
	Numero* array = new Numero[x];
	/*Validación del tamaño del arreglo. */
	if(x > 1000000 or x <= 0){
		cout<< "Parámetro Numérico NO válido." << endl;
		return 0;
	}
	
	/*Validación del parámetro ver */
	if (ver == "s" or ver == "S"){
		/*Si la variable 'verconfirmacion' es distinta de NULL, el usuario desea ver los tiempos*/
		verConfirmacion = 1; 
		control(array, x, verConfirmacion);
	
	}
	else if (ver == "n" or ver == "N"){
		verConfirmacion = 0;
		control(array, x, verConfirmacion);
	}
	else {
		cout << "[No ingresó el parámetro VER]" << endl;
		return 0;
	}
	return 0;
}
