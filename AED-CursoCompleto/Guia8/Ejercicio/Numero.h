#include <iostream>
using namespace std;

#ifndef NUMERO_H
#define NUMERO_H

class Numero {
    private:
		int valor = 0;
       
    public:
    
		/* constructor */
		Numero ();
		/* get y setter valor*/
		int get_valor();
		void set_valor(int valor);

};
#endif
