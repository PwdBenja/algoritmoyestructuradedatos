- GUIA 8
El programa utilizará los diferentes métodos de ordenamiento, como lo son:

-> Burbuja Menor
->Burbuja Mayor
->Insercion
->Insercion Binaria 
->Seleccion
->Shell
->Quicksort
-----------------------------------
con el propósito de rellenarlos aleatoriamente con datos que varian entre el 1 hasta al 10 de forma desordenada y que lo ordenen calculando sus milisegundos.


-El programa se compila utilizando el comando make y se ejecuta con ./main "Tamaño del arreglo" "VER"
-El programa sólo funcionará si el parámetro ver es S, s, n, N si usted desea ver los datos DESORDENADOS y el tiempo, es N. En cambio si desea ver los datos
dentro del arreglo debe ser S. 
->REQUISITOS PREVIOS
Sistema operativo Linux 
GNU Make (para compilar) 

->CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.

->VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

->AUTORES

Benjamín Astudillo - Desarrollo del código, ejecución de proyecto y narración de README. 

->EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

