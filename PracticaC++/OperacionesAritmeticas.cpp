/*
Escribe un programa que lea de la entrada estándar de dos números y muestre en la salida estándar su suma, resta, multiplicación y división.

*/


#include <iostream> 

using namespace std; 

void sumar (int a, int b){
    int suma = 0;
    suma = (a + b);
    cout << "La suma es: " << suma << endl;
}

void resta(int a, int b){
    int resta = 0;
    resta = (a - b);
    cout << "La resta es: " << resta << endl;
}


void OperacionesAritmeticas(int numero, int numeroDos){
    int a, b;
    a = numero;
    b = numeroDos;
    cout << "Operaciones  \n";
    cout << "Sumar      |0|  \n";
    cout << "Restar     |1|  \n";
    int opcion;
    cin >> opcion;
    if (opcion == 0){
        sumar(a, b);
    }
    else if (opcion == 1){
        resta(a,b);
    }
    else{
        OperacionesAritmeticas(numero,numeroDos);
    }
}

int main () {
    int numero = 0, numeroDos = 0;
    cout << "ingrese numero: \n";
    cin >> numero;
    cout << "ingrese numero: \n";
    cin >> numeroDos;
    OperacionesAritmeticas(numero, numeroDos);
     
    return 0; 
}
